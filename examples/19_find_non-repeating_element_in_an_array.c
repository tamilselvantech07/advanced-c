#include<stdio.h>
#include<stdbool.h>


void find_non_repeating_element(int *array, int length); // Declaration

int main(void)
{
  int myarray[] = {7,8,3,4,5,6,8,7,9,1};

  int len = sizeof(myarray)/sizeof(myarray[0]);
  find_non_repeating_element(myarray, len);

	return 0;
}

void find_non_repeating_element(int *array, int length)
{
	printf("Length:%d\n", length);
  bool repeating = false;
  bool found_non_repeating = false;

	for(int i=0; i < length; i++)
	{
		repeating = false;
		for(int j=0; j < length; j++)
		{
			//printf("array[%d, %d]=(%d %d)\n", i, j, array[i], array[j]); 
			if(array[i] == array[j] && i != j)
			{
				repeating = true;
				break;  // It will break the j loop only.
			}
		}

		if(!repeating)
		{
			printf("Non repeating Element: %d\n", array[i]);
			found_non_repeating = true;
		}
	}
	if(!found_non_repeating)
	{
		printf("No non-repeating element\n");
	}
}

