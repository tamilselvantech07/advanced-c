#include<stdio.h>
#include<stdbool.h>

bool primeChecker(int n, int i);
bool primeChecker2(int n, int stop, int i);
bool isPrime(int n);

// Method 1
bool primeChecker(int n, int i)
{
    if (n <=1) return false;
    else if (n == 2) return true;
    else if (n%i == 0) return false;
    else if (i >= n/2) return true;
    else return primeChecker(n, i+1);
}

// Method 2
bool primeChecker2(int n, int stop, int i)
{
    if(n%i==0) return false;
    else if(i>= stop) return true;
    else return primeChecker2(n, stop, i+1);
}

bool isPrime(int n)
{
    if (n<=1) return false;
    else if(n==2) return true;
    else return primeChecker2(n, n/2, 2);
}

int main()
{
  // Method 1
  if(primeChecker(7, 2)) printf("7 is prime\n");
  else printf("7 is not prime\n");

  // Method 2
  if(isPrime(11)) puts("11 is prime");
  else puts("11 is not prime");
  return 0;
}
