#include <stdio.h>

void reverse(int array[], int low, int high);

int main()
{
    int nums[] = {1,2,3,4,5,6};
    int length = sizeof(nums) / sizeof(nums[0]);

    reverse(nums, 0, length - 1);
    
    for(int i=0; i<length; i++)
        printf("%d", nums[i]);
   
    puts("");
    return 0;
}

void reverse(int array[], int low, int high)
{
    if(low < high)
    {
        int temp = array[low];
        array[low] = array[high];
        array[high] = temp;
        reverse(array, low+1, high-1);
    }
}