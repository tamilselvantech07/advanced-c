#include<stdio.h>
#include<string.h>
#include<ctype.h>

#include<stdlib.h>

int vowel_count(char *s);
char *string_append(char *str1, char *str2);

int vowel_count(char *s)
{
	int count = 0;
	unsigned long int i;
	for(i = 0; i < strlen(s); i++)
	{
		switch (toupper(s[i]))
		{
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				count++;
		}
	}
	return count;
}

char *string_append(char *str1, char *str2)
{
	int size = strlen(str1) + strlen(str2) + 1;

	char *str = calloc(size, sizeof(char));

  unsigned long int i;
	for(i=0; i < strlen(str1); i++)
		str[i] = str1[i];

	for(i=0; i < strlen(str2); i++)
		str[strlen(str1) + i] = str2[i];

	str[size - 1] = '\0';

	return str;
}

int main(void)
{
	char s1[] = "It's a wonderful life!";

	printf("Number of vowels: %d\n", vowel_count(s1));
	
	// String append
	puts("\nString Append");
	char first[20] = "First";
	char second[20] = "Second";

  strcat(first, second);
	printf("first: %s\n", first);
	printf("strlen of first: %ld\n", strlen(first));

  char str1[] = "abc";
  char str2[] = "wxyz";
	char *string = string_append(str1, str2);
	printf("Appended string: %s\n", string);
	free(string);

	return 0;
}
