#include<stdio.h>

void reverse(char *str);

int main()
{
    char a[] = "hello";
    reverse(a);  // you can't directly pass string reverse("hello")
    printf("%s\n", a);
    return 0;
}

void reverse(char *str)
{
    if(str == NULL) return;

    char *end = str;

    if(*end == '\0') return;
    
    while(*end != '\0') end++;
    end--;

    char *start = str;

    char temp;
    while(start < end)
    {
        temp = *start;
        *start = *end;
        *end = temp;
        
        start++;
        end--;
    }

}