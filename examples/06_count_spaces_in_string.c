#include<stdio.h>
#include<string.h>

int count_spaces(char *string); // Declartion

int main(void)
{
  char string[] = "Live lifee to the fullest";

  int count = count_spaces(string);
	printf("No of spaces: %d\n", count);
	return 0;
}

int count_spaces(char *a)
{
  int length = strlen(a);
  int count = 0;
	for(int i=0; i<length; i++)
	{
		if(a[i] == ' ')
			count++;
	}
	return count;
}
