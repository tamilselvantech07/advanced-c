#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define LOG(format, ...) { \
	time_t curtime = time(NULL); \
	struct tm *ltm = localtime(&curtime);\
	printf("[%d-%02d-%02d %02d:%02d:%02d] %s:%s():%d " format, \
        ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, \
        ltm->tm_hour, ltm->tm_min, ltm->tm_sec, \
	    __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
}

int main(void)
{
  int nums[] = {1,2,3,4,5,6,7,8,9};
  int length = sizeof(nums)/sizeof(nums[0]);

	int temp;

	for(int i=0; i<(length/2); i++)
	{
	  temp = nums[i];
	  nums[i] = nums[length-i-1];
	  nums[length-i-1] = temp;
	}

	for(int j=0; j<length; j++)
	{
	  //printf("%d\n", nums[j]);
	  LOG("%d\n", nums[j]);
	}
	printf("%d\n", *nums);

	return 0;
}
