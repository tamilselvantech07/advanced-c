#include<stdio.h>

void reverse(char *str);
void reverse2(char *str, int index, int n);

int main()
{
	// Method 1
	char a[] = "Geeks program";
	reverse(a);
	printf("\n");

	// Method 2
	char b[] = "Geeks for Geeks";
	int n = sizeof(b) / sizeof(b[0]);
	reverse2(b, 0, n);
	printf("\n");

	return 0;
}

// print G
 // print e
  // print e
   // print k

void reverse(char *str)
{
	if(*str)   // (*str != '\0')
	{
		reverse(str + 1);  // basically creating a stack.
		printf("%c", *str);
	}
}

void reverse2(char *str, int index, int n)
{
	// return if we reached at last index or 
	// at the end of the string
	if(index == n)
		return;

	// storing each character starting from index 0 
	// in function call stack;
	char temp = str[index]; 

	// calling recursive function by increasing 
	// index everytime
	reverse2(str, index + 1, n); 

	// printing each stored character while 
	// recurring back
	printf("%c", temp);			 
}
