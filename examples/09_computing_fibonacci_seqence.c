#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	int t1 = 0;
	int t2 = 1;
	int limit;

	printf("Enter the limit of the fibonacci sequence:");
	int s = scanf("%u", &limit);
  if(s!=0)
  {
    printf("\nReading a limit is failed\n");
    exit(1);
  }

	printf("%d ,%d, ", t1, t2);
	int next = t1 + t2;
	for(int i=0; next<limit; i++)
	{
		printf("%d, ", next);
		t1 = t2;
		t2 = next;
		next = t1 + t2;
	}
	puts("");
	return 0;
}
