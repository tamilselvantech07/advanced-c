#include<stdio.h>

int main()
{
  double arr[] = {5.2, 9.3, 6.5, 4.1, 7.8};

  double sum = 0;
  int length = sizeof(arr)/sizeof(arr[0]);
  for(int i=0; i<length; i++)
  {
    sum += arr[i];
  }
  printf("Average of arr:%.3lf\n", (double)sum/length);

  return 0;
}
