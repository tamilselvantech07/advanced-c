#include<stdio.h>

unsigned long int factorial(int n);

int main(void)
{
    for(int i=1; i<=20; i++)
    {
      printf("factorial(%d) = %lu\n", i, factorial(i));
    }
    return 0;
}

unsigned long int factorial(int n)
{
    if (n==1) return 1;
    else return n * factorial(n-1);   // It is not like 5 * 4 * 3 ..
    // It will first get 1 * 2 * 3 * 4 * 5 = 120;
}