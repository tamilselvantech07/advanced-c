#include<stdio.h>
#include<stdlib.h>

#include<unistd.h> //getcwd()
#include<errno.h> // set errno in the errno.h lib if an error occurs

int main(void)
{
  char buffer[50];
	char *retptr = getcwd(buffer, 50);

	//char *retptr = getcwd(NULL, 1024);

	if (retptr == NULL)
	{
		puts("Get current working directory failed");
		
		if(errno == ERANGE)
			puts("Path exceeds max buffer length.\n");
		else if(errno == ENOMEM)
			puts("Memory cannot be allocated for path.\n");

		return 1;
	}

	printf("Currently working directory: %s \n", buffer);
	//free(retptr);

	return 0;
}
