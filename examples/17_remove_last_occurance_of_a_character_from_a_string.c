#include <stdio.h>
#include <string.h>

void remove_last(char string[], const char to_remove);

int main(void)
{

  //               0123456789
	//                        \0
	char string[] = "Microsoft";
	char to_remove = 'o';


  remove_last(string, to_remove);

	printf("Char to remove = %c\n", to_remove);
	printf("%s\n", string);

	return 0;
}

void remove_last(char string[], const char to_remove)
{
  int length = strlen(string);
	int i = length - 1;
	while(i >= 0)
	{
		if(string[i] == to_remove) break;
		i--;
	}
	
	if( i >= 0)
	{
		while(i < length)
		{
			string[i] = string[i+1];
			i++;
		}
	}
}
