#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include<stdlib.h>
#include"../advanced/advanced_debugging/debug.h"

bool is_palindrome(char string[]);
bool isPalindromeRecursion(char str[], int l, int h);

bool is_palindrome(char string[])
{
	bool rv = false;
	int length = strlen(string);
	LOG(1, "length of %s is %d\n", string, length);
	LOG(1, "MID length:%d\n", length/2); 
	for(int i=0; i<length/2; i++)
	{
		LOG(1, "string[%c] == string[%c]\n",string[i], string[length-i-1]);
		if(string[i] == string[length-i-1])
			rv = true;
		else
			rv = false;
	}

	return rv;
}

bool isPalindromeRecursion(char str[], int l, int h)
{
    // if the string size is 0 or 1
    // it is a palindrome
    if (l >= h)
        return true;

    // if both the terminal characters
    // not matching, not palindrome
    if (str[l] != str[h])
        return false;

    // call the smaller sub-problem
    return isPalindromeRecursion(str, l + 1, h - 1);
}


int main(void)
{
	char string1[] = "tamilselvan!";
	char string2[] = "abcdedcba";

	if(is_palindrome(string1))
		printf("%s is palindrome\n", string1);
	else
		printf("%s is Not palindrome\n", string1);

	if(is_palindrome(string2))
		printf("%s is palindrome\n", string2);
	else
		printf("%s is Not palindrome\n", string2);

	// Method 2: recursion
	char str[10];
	printf("Enter a string:");
	int s = scanf("%s", str);
  if(s != 0)
  {
    printf("\nReading a string is failed\n");
    exit(1);
  }
	// Start from first and
	// last character of str
	int l = 0;
	int h = strlen(str) - 1;
	bool ans = isPalindromeRecursion(str, l, h);
	if (ans)
		printf("%s is a palindrome\n", str);
	else
		printf("%s is not a palindrome\n", str);

	return 0;
}
