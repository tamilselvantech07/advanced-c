#include <stdio.h>
#include <stdbool.h>

bool string_equals(const char *s1, const char *s);

int main(void)
{
	char string1[] = "ABCD";
	char string2[] = "ABC";

	bool rv = string_equals(string1, string2);
	printf("is string are equal: %s\n", rv ? "True" : "False");
	return 0;
}

bool string_equals(const char *s1, const char *s2)
{
	if(*s1 != *s2) return false;
	else if(*s1 == '\0' && *s2 == '\0') return true;
	else return string_equals(++s1, ++s2);
}
