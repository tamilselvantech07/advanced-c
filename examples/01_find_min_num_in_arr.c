#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define debug_print(str_format, ...) { \
    time_t curtime=time (NULL); \
    struct tm *ltm = localtime (&curtime); \
    printf("[%d-%02d-%02d %02d:%02d:%02d] " str_format, \
        ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, \
        ltm->tm_hour, ltm->tm_min, ltm->tm_sec, ##__VA_ARGS__); \
}
   

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y)) 

int find_len_arr(int array[]);

int find_len_arr(int array[])
{
	int count = 0;
	for(int i=0;array[i] != '\0'; i++)
	{
		count++;
	}
	return count;
}

int main(void)
{

	int array[10] = {50,20,40,10,30,90,70,60,80,100};

	int min = array[0]; // set the 1st elem as min & max;
	int max = array[0]; 

        printf("MIN(5,4): %d\n", MIN(5,4));
        printf("MAX(5,4): %d\n", MAX(5,4));

	int len_of_arr = sizeof(array)/sizeof(array[0]); //O(1) O(1)

	//int len_arr_2 = *(&array + 1) - array;  // array var is the 1st index elem address

	//int len_arr_3 = find_len_arr(array); // O(n) O(1)

	printf("length of array: %d\n", len_of_arr);

	for(int i=0; i<len_of_arr; i++)
	{
		debug_print("Current element: %d\n", array[i]);
		if (array[i] < min)
		{
			min = array[i];
		    debug_print("Current min: %d\n", min);
		}
		else if (array[i] > max)
		{
			max = array[i];
		    debug_print("Current max: %d\n", max);
		}
	}


	printf("Minimum number in the array: %d\n", min);
	printf("Maximum number in the array: %d\n", max);

}
