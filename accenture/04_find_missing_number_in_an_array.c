#include <stdio.h>

int missingNumber(int *array, int size);

int main(void)
{
	puts("Enter a sorted Array ");
	int array[10];
	for(int i = 0; i < 10;  i++)
		scanf("%d", &array[i]);

  int n = sizeof(array)/sizeof(array[0]);
  int num = missingNumber(array, n);
	printf("Missing Number: %d\n", num);
	return 0;
}

int missingNumber(int *arr, int n)
{
    int sum = 0;
    // Calculate the sum of array elements
    for (int i = 0; i < n - 1; i++) {
        sum += arr[i];
    }

    // Calculate the expected sum
    int expectedSum = (n * (n + 1)) / 2;

    // Return the missing number
    return expectedSum - sum;
}
