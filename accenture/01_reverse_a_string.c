#include<stdio.h>
#include<string.h>

void reverseString(char *str);
void reverseStringRecursion(char *str); 
void reverseStringRecursion2(char *str, int index, int n);
void reverseStringArray(char *str);

int main(void)
{
	char *string;
	int n;
	printf("Enter an input:");
	scanf("%s", string);
  
	n = sizeof(string)/sizeof(string[0]);

	// Method 1
	reverseString(string);
	printf("reverseString: %s\n", string);
	reverseString(string);

  // Method 2
	printf("reverseStringRecursion:");
	reverseStringRecursion(string);
	puts("");

	// Method 3
	printf("reverseStringRecursion2:");
	reverseStringRecursion2(string, 0, n);
	puts("");
	
  // Method 4
	reverseStringArray(string);
	printf("reversedStringArray: %s\n", string);
  //printf("Reversed String: %s", strrev(string));
	return 0;
}

void reverseString(char* str) {

    // Initialize start and end pointers
    int start = 0;
    int end = strlen(str) - 1;
    char temp;

    // Swap characters while start is less than end
    while (start < end) {
      
        // Swap characters
        temp = str[start];
        str[start] = str[end];
        str[end] = temp;

        // Move pointers
        start++;
        end--;
    }
}

void reverseStringRecursion(char *str)
{
	if(*str != '\0') 
	{
		reverseStringRecursion(str + 1); // basically creating a stack
		printf("%c", *str);
	}
}


void reverseStringRecursion2(char *str, int index, int n)
{
	// return if we reached at last index or 
	// at the end of the string
	if(index == n)
		return;

	// storing each character starting from index 0 
	// in function call stack;
	char temp = str[index]; 

	// calling recursive function by increasing 
	// index everytime
	reverseStringRecursion2(str, index + 1, n); 

	// printing each stored character while 
	// recurring back
	printf("%c", temp);			 
}


void reverseStringArray(char *str)
{
	// Temp array assuming the max size of the original
	// string 100
	char temp[100];

	// Size pointer of the temp array
	int t_size = -1;
	int len = strlen(str);

	// Push all characters of the string onto the temp
	for (int i = 0; i < len; i++) {
		temp[++t_size] = str[i];
	}

	// Copying all characters from the temp array and
	// replace in the original string
	for (int i = 0; i < len; i++) {
		str[i] = temp[t_size--];
	}
}
