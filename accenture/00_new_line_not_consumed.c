#include <stdio.h>
#include <string.h>

int main(void) {
	int num = 0;
	char str[128], *lf;
	scanf("%d", &num);
  //while ((c = getchar()) != '\n' && c != EOF);
	fgets(str, sizeof(str), stdin);
	if ((lf = strchr(str, '\n')) != NULL) *lf = '\0';
	printf("%d \"%s\"\n", num, str);
	return 0;
}

/*******************************************************************************
42
life

the output will be 42 "" instead of expected 42 "life".
This is because a newline character after 42 is not consumed in the call of 
scanf() and it is consumed by fgets() before it reads life.
Then, fgets() stop reading before reading life.


Another way is to read until you hit a newline character after using scanf()
and before using fgets().

while ((c = getchar()) != '\n' && c != EOF);
*******************************************************************************/
