#include <stdio.h>

int findmax(int *array, int size);
void printArray(int *values);

int main(void)
{
 int values[5];

  printf("Enter 5 integers: ");

  // taking input and storing it in an array
  for(int i = 0; i < 5; ++i) {
     scanf("%d", &values[i]);
  }
  int size = sizeof(values) / sizeof(values[0]);
  int max = findmax(values, size);
	printf("Max of array: %d\n", max);
	printArray(values);
  return 0;
}

int findmax(int *array, int n)
{
  int max = array[0];
	for(int i=0; i<n; i++)
	{
		printf("%d\n", array[i]);
		if(max < array[i])
			max = array[i];
	}
	return max;
}

void printArray(int *values)
{
  printf("Displaying integers: ");

  // printing elements of an array
  for(int i = 0; i < 5; ++i) {
     printf("%d\n", values[i]);
  }
}
