#include <stdio.h>
#include <string.h>
#include <stdbool.h>

bool isPalindromeRecursion(char str[], int l, int h);
int isPalindrome(char str[]);

int isPalindrome(char str[])
{
	int low = 0;
	int high = strlen(str) - 1;

	// Keep comparing characters while they are same
	while (low < high)
	{
		if (str[low] != str[high])
		{
			return 0; // not a palindrome.
		}
		low++; // move the low index forward
		high--; // move the high index backwards
	}
	return 1; // is a palindrome
}

bool isPalindromeRecursion(char str[], int l, int h)
{
    // if the string size is 0 or 1
    // it is a palindrome
    if (l >= h)
        return true;

    // if both the terminal characters
    // not matching, not palindrome
    if (str[l] != str[h])
        return false;

    // call the smaller sub-problem
    return isPalindromeRecursion(str, l + 1, h - 1);
}

int main()
{
	  char *str;
		printf("Enter a string:");
		scanf("%s", str);
    // Start from first and
    // last character of str
    int l = 0;
    int h = strlen(str) - 1;

    bool ans = isPalindromeRecursion(str, l, h);
    if (ans) {
        printf("%s is a palindrome\n", str);
    }
    else {
        printf("%s is not a palindrome\n", str);
    }
		
		// Another Method
		printf("%s is %s palindrome\n", str, isPalindrome(str) ? "a" : "not");

    return 0;
}

