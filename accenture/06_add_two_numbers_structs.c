#include<stdio.h>
#include<stdlib.h>

 struct ListNode {
      int val;
      struct ListNode *next;
 };

struct ListNode* createNode(int value) {
    struct ListNode* newNode = (struct ListNode*)malloc(sizeof(struct ListNode));
    newNode->val = value;
    newNode->next = NULL;
    return newNode;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    signed long i = 1, j = 1;
    signed long long t1, t2;
    struct ListNode *c1 = l1;
    while(c1 != NULL)
    {
      t1 += c1->val * i; 
      i = i*10;
      c1 = c1->next;
    }

    struct ListNode *c2 = l2;
    while(c2 != NULL)
    {
      t2 += c2->val * j; 
      j = j*10;
      c2 = c2->next;
    }
    signed long long sum = t1+t2;
		printf("%lld\n", sum);

    struct ListNode* head = NULL;
    struct ListNode* tail = NULL;
    while(sum > 0)
    {
        signed long long digit = sum % 10;
        struct ListNode* newNode = createNode(digit);  // Create a new node
        if (head == NULL) {
            // If the list is empty, the new node becomes the head
            head = newNode;
            tail = head;
        } else {
            // Otherwise, append the new node to the end of the list
            tail->next = newNode;
            tail = newNode;
        }
        sum = sum/10;
    }
    return head;
}

void printList(struct ListNode* head) {
		struct ListNode* current = head;
    while (current != NULL) {
        printf("%d ", current->val);
        current = current->next;
    }
    printf("\n");
}


int main(void)
{
	struct ListNode *l1 = createNode(9);

	struct ListNode *l2 = createNode(1);
	l2->next = createNode(9);
	l2->next->next = createNode(9);
	l2->next->next->next = createNode(9);
	l2->next->next->next->next = createNode(9);
	l2->next->next->next->next->next = createNode(9);
	l2->next->next->next->next->next->next = createNode(9);
	l2->next->next->next->next->next->next->next = createNode(9);
	l2->next->next->next->next->next->next->next->next = createNode(9);
	l2->next->next->next->next->next->next->next->next->next = createNode(9);

  printList(l1);
	printList(l2);
	struct ListNode *rl = addTwoNumbers(l1, l2);
	printList(rl);
  return 0;
}
