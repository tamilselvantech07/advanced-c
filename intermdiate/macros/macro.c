#include "main.h"

// #define MACRO_NAME macro_body
// Here, MACRO_NAME is a C identifier and macro_body is string/value/expression.

// Macro definition
#define COUNTRYI "INDIA"        // String constant
#define TRUE    1               // Integer constant
#define FALSE   0               // Integer constant
#define SUM (10 + 20)           // Macro definition

#define SWAP(x, y, temp) temp = x; x=y; y=temp;
#define SQUARE(a) (a*a);

int main()
{
	int a,b,temp;
	printf("Enter 2 inter numbers \n");
	scanf("%d%d", &a, &b);

	printf("Before swapping a = %d and b = %d\n", a, b);

	SWAP(a, b, temp);
	printf("After swapping a = %d and b  = %d\n", a, b);

	int c = SQUARE(a);
	printf("Square of c = %d\n", c);

  // Macro from Header File
	int x=43;
	int y=10;

	PRNT(x,y);// actually the macro is a text substitution technique and it will
	          // be replaced by the content defined for its name in its definition

	return 0;
}
