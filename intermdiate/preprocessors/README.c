/*******************************************************************************
 * Preprocessor Directives	and Description
 ******************************************************************************/

#define
Used to define a macro

#undef
Used to undefine a macro

#include
Used to include a file in the source code program

#ifdef
Used to include a section of code if a certain macro is defined by #define

#ifndef
Used to include a section of code if a certain macro is not defined by #define

#if
Check for the specified condition

#else
Alternate code that executes when #if fails

#endif
Used to mark the end of #if, #ifdef, and #ifndef



/*******************************************************************************
 * These preprocessors can be classified based on the type of function they
 * perform.
 *
 * There are 4 Main Types of Preprocessor Directives:
 ******************************************************************************/

1. Macros
2. File Inclusion
3. Conditional Compilation
4. Other directives

