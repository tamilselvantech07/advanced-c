#include<stdio.h>

#if 0
/****************************************************************************** 
 * The Macro Continuation (\) Operator
 * A macro is normally confined to a single line. The macro continuation 
 * operator(\) is used to continue a macro that is too long for a single line.
 * For example −
 ******************************************************************************/

#define  message_for(a, b)  \
   printf(#a " and " #b ": We love you!\n")


#endif

#if 0
/*******************************************************************************
 * The Stringize (#) Operator
 * The stringize or number-sign or pound operator ( '#' ), when used within a
 * macro definition, converts a macro parameter into a string constant.
 * This operator may be used only in a macro having a specified argument or 
 * parameter list. For example −
 ******************************************************************************/

#undef message_for

#define  message_for(a, b)  \
   printf(#a " and " #b ": We love you!\n")

int main(void) {
   message_for(Carole, Debra);
   return 0;
}

// o/p: Carole and Debra: We love you!
#endif


/*****************************************************************************
 * The Token Pasting (##) Operator
 * The token-pasting operator (##) within a macro definition combines two 
 * arguments. It permits two separate tokens in the macro definition to be
 * joined into a single token. For example −
 ******************************************************************************/

#define tokenpaster(n) printf ("token" #n " = %d", token##n)

int main(void) {
   int token34 = 40;
   tokenpaster(34);
   return 0;

// printf ("token34 = %d", token34);   // o/p:  token34 = 40;
// This example shows the concatenation of token##n into token34 and
// here we have used both stringize and token-pasting. 
}


#if 0
/*******************************************************************************
  Macros with arguments must be defined using the #define directive before they
  can be used. The argument list is enclosed in parentheses and must immediately
  follow the macro name. Spaces are not allowed between the macro name and open 
  parenthesis. For example −
*******************************************************************************/
#define MAX(x,y) ((x) > (y) ? (x) : (y))

int main(void){
	printf("Max between 20 and 10 is %d\n", MAX(10, 20));
	return 0;
}
#endif
