#include <stdio.h>

//#include "main.h"
//#include "win/display.c"

// Here, file “stdio.h” is a standard header file, 
// “main.h” and “win/display.c” is custom C files.

//  We use this variant of including file when we want to include our own/custom header file. It searches the file in the current directory and then in the standard header file’s directory.

// #define MACRO_NAME macro_body
// Here, MACRO_NAME is a C identifier and macro_body is string/value/expression.


// Macro definition
#define COUNTRYI "INDIA"        // String constant
#define TRUE    1               // Integer constant
#define FALSE   0               // Integer constant
#define SUM (10 + 20)           // Macro definition

#define TRUE 1
#define FALSE 0

#define COUNTRYI "INDIA"

#define IND 1
#define USA 2
#define UK  3

#define COUNTRY IND


int main()
{
	printf("COUNTRY: %s\n", COUNTRYI);
	printf("TRUE: %d\n", TRUE);
	printf("FALSE: %d\n", FALSE);
	printf("SUM(10 + 20): %d\n", SUM);


	printf("TRUE: %d\n", TRUE);
	printf("FALSE: %d\n", FALSE);

	// Undefine a previously defined macro
#undef TRUE
#undef FALSE

	// Re-define macro values
#define TRUE 0
#define FALSE 1

	printf("\nMacro values are redefinition\n");
	printf("TRUE: %d\n", TRUE);
	printf("FALSE: %d\n", FALSE);


	// If COUNTRY is defined, print a message
#ifdef COUNTRYI
	printf("\nCountry is defined\n");
#endif

	// If STATE is not defined, define it
#ifndef STATE
	printf("State is not defined. Defining state. \n");
#define STATE "PATNA"
#endif

	printf("State is: %s\n", STATE);



#if COUNTRY == IND
	printf("\nSelected country code is: %d\n", COUNTRY);
	// Do some task if country is India
#elif COUNTRY == USA
	printf("Selected country code is: %d\n", COUNTRY);
	// Do some task if country is USA
#else
	printf("Selected country code is: %d\n", COUNTRY);
	// Do some task if country is UK
#endif

	return 0;
}

