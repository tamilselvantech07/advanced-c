/*
 3. Searching in Binary Tree
Searching for a value in a binary tree means looking through the tree to find
a node that has that value. Since binary trees do not have a specific order
like binary search trees, we typically use any traversal method to search.
The most common methods are
  depth-first search (DFS) and
  breadth-first search (BFS).
In DFS, we start from the root and explore the depth nodes first.
In BFS, we explore all the nodes at the present depth level before moving on to
the nodes at the next level. We continue this process until we either find the
node with the desired value or reach the end of the tree. If the tree is empty
or the value isn’t found after exploring all possibilities, we conclude that
the value does not exist in the tree.

*/

#include <stdio.h>
#include <stdlib.h>

struct Node *newNode(int);
int searchDFS(struct Node *, int);

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*
                        )malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = NULL;
    return node;
}

// Function to search for a value in the binary tree using DFS
int searchDFS(struct Node* root, int value) {
    // Base case: If the tree is empty or we've reached a leaf node
    if (root == NULL) return 0;

    // If the node's data is equal to the value we are searching for
    if (root->data == value) return 1;

    // Recursively search in the left and right subtrees
    int left_res = searchDFS(root->left, value);
    int right_res = searchDFS(root->right, value);
    
    return left_res || right_res;
}

int main() {
    struct Node* root = newNode(2);
    root->left = newNode(3);
    root->right = newNode(4);
    root->left->left = newNode(5);
    root->left->right = newNode(6);

    int value = 6;
    if (searchDFS(root, value))
        printf("%d is found in the binary tree\n", value);
    else
        printf("%d is not found in the binary tree\n", value);

    return 0;
}

