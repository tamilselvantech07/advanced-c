/*
2. Insertion in binary tree
Inserting elements means add a new node into the binary tree. As we know that
there is no such ordering of elements in the binary tree, So we do not have to
worry about the ordering of node in the binary tree.

We would first creates a root node in case of empty tree. Then subsequent
insertions involve iteratively searching for an empty place at each level of
the tree.
When an empty left or right child is found then new node is inserted there.
By convention, insertion always starts with the left child node.

*/
#include <stdio.h>
#include <stdlib.h>
#include "../../../advanced/advanced_debugging/debug.h"

struct Node *createNode(int);
struct Node *insert(struct Node *, int);
void inorder(struct Node *);

struct Node {
  int data;
  struct Node *left;
  struct Node *right;
};

struct Node *createNode(int key) {
  struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
  newNode->data = key;
  newNode->left = NULL;
  newNode->right = NULL;
  return newNode;
}

// Function to insert a new node in the binary tree
struct Node *insert(struct Node *root, int key) {
  if (root == NULL) return createNode(key);
  printf("Inserting key = %d\n", key);

  // Create a queue for level order traversal
  struct Node *queue[100];
  int front = 0, rear = 0;
  queue[rear++] = root;

  while (front < rear) {
    struct Node *temp = queue[front++];

    // If left child is empty, insert the new node here
    if (temp->left == NULL) {
      temp->left = createNode(key);
      break;
    } else {
      queue[rear++] = temp->left;
    }

    // If right child is empty, insert the new node here
    if (temp->right == NULL) {
      temp->right = createNode(key);
      break;
    } else {
      queue[rear++] = temp->right;
    }
  }
  return root;
}

void inorder(struct Node *root) {
    if (root == NULL) return;
    inorder(root->left);
    printf("%d ", root->data);
    inorder(root->right);
}

int main(void) {
  struct Node *root = createNode(2);
  root->left = createNode(3);
  root->right = createNode(4);
  root->left->left = createNode(5);

  printf("Inorder traversal before insertion: ");
  inorder(root);
  printf("\n");

  int key = 6;
  root = insert(root, key);

  printf("Inorder traversal after insertion: ");
  inorder(root);
  printf("\n");

  return 0;
}

