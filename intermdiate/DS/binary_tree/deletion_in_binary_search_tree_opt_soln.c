/*
we are going to discuss an optimization over the previous implementation.
The optimization is mainly for the case when both left and right children are
not empty for a node to be deleted. In the earlier implementation, we followed
the following steps for this case

1) Find the inorder successor
2) Replace the node’s key with the successor’s key
3) Delete the successor using a recursive call to the delete method in the right
subtree

With above steps, we do two traversals of the right subtree (in step 1 and 3).
We can achieve the task with one traversal only and avoid a recursive call.
While finding the inorder successor, we keep track of the parent of the
successor also. With the help of parent pointer . reference, we can delete the
node without making a recursive call. Below is the implementation of the idea.

*/

#include <stdio.h>
#include <stdlib.h>

struct Node {
    int key;
    struct Node* left;
    struct Node* right;
};

/* Given a binary search tree and a key, this
   function deletes the key and returns the
   new root */
struct Node* delNode(struct Node* root, int k) {
    // Base case
    if (root == NULL) {
        return root;
    }

    // If the key to be deleted is smaller than
    // the root's key, then it lies in the left
    // subtree
    if (k < root->key) {
        root->left = delNode(root->left, k);
        return root;
    }

    // If the key to be deleted is greater
    // than the root's key, then it lies in
    // the right subtree
    else if (k > root->key) {
        root->right = delNode(root->right, k);
        return root;
    }

    // If key is same as root's key, then this
    // is the node to be deleted
    // Node with only one child or no child
    if (root->left == NULL) {
        struct Node* temp = root->right;
        free(root);
        return temp;
    } else if (root->right == NULL) {
        struct Node* temp = root->left;
        free(root);
        return temp;
    }

    // Node with two children: Get the inorder
    // successor (smallest in the right subtree).
    // Also find parent of the successor
    struct Node* succParent = root;
    struct Node* succ = root->right;
    while (succ->left != NULL) {
        succParent = succ;
        succ = succ->left;
    }

    // Copy the inorder successor's content
    // to this node
    root->key = succ->key;

    // Delete the inorder successor
    if (succParent->left == succ) {
        succParent->left = succ->right;
    } else {
        succParent->right = succ->right;
    }

    free(succ);
    return root;
}

// Utility function to do inorder traversal
void inorder(struct Node* root) {
    if (root != NULL) {
        inorder(root->left);
        printf("%d ", root->key);
        inorder(root->right);
    }
}

struct Node* createNode(int key) {
    struct Node* newNode =
      (struct Node*)malloc(sizeof(struct Node));
    newNode->key = key;
    newNode->left = newNode->right = NULL;
    return newNode;
}

// Driver code
int main(void) {
    struct Node* root = createNode(10);
    root->left = createNode(5);
    root->right = createNode(15);
    root->right->left = createNode(12);
    root->right->right = createNode(18);
    int x = 15;

    printf("      10\n");
    printf("     \/  \\ \n");
    printf("    5    15\n");
    printf("         \/ \\  \n");
    printf("        12 18\n\n");
    inorder(root);
    root = delNode(root, x);
    inorder(root);
    printf("\n");
    return 0;
}

