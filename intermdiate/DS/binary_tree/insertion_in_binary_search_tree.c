/*

Binary Search Tree:
===================
A Binary Search Tree is a data structure used in computer science for
organizing and storing data in a sorted manner.
Each node in a Binary Search Tree has at most two children, a left child and
a right child, with the left child containing values less than the parent node
and the right child containing values greater than the parent node.

Important:
This hierarchical structure allows for efficient searching, insertion, and
deletion operations on the data stored in the tree.

AKA:
What is Binary Search Tree?
Binary Search Tree (BST) is a special type of binary tree in which the left
child of a node has a value less than the node’s value and the right child has
a value greater than the node’s value. This property is called the BST property
and it makes it possible to efficiently search, insert, and delete elements in
the tree.

Properties of Binary Search Tree:
The left subtree of a node contains only nodes with keys lesser than the
node’s key.
The right subtree of a node contains only nodes with keys greater than
the node’s key.
The left and right subtree each must also be a binary search tree.  
There must be no duplicate nodes(BST may have duplicate values with different
handling approaches).

*/

#include <stdio.h>
#include <stdlib.h>

// Define the structure for a BST node
struct Node {
  int key;
  struct Node *left;
  struct Node *right;
};

// Function to create a new BST node
struct Node *newNode(int item) {
  struct Node *temp = 
    (struct Node*)malloc(sizeof(struct Node));
  temp->key = item;
  temp->left = temp->right = NULL;
  return temp;
}


// Function to insert a new node with the given key
struct Node *insert(struct Node *node, int key) {

  // If the tree is empty, return a new node
  if (node == NULL)
    return newNode(key);

  // If the key is already present in the tree,
  // return the node
  if (node->key == key)
    return node;

  // Otherwise, recur down the tree. If the key 
  // to be inserted is greater than the node's key,
  // insert it in the right subtree
  if (node->key < key)
    node->right = insert(node->right, key);

  // If the key to be inserted is smaller than 
  // the node's key,insert it in the left subtree
  else
    node->left = insert(node->left, key);

  // Return the (unchanged) node pointer
  return node;
}

// Function to perform inorder tree traversal
void inorder(struct Node *root) {
    if (root != NULL) {
        inorder(root->left);
        printf("%d ", root->key);
        inorder(root->right);
    }
}

// Driver program to test the above functions
int main(void) {
    // Creating the following BST
    //      50
    //     /  \
    //    30   70
    //   / \   / \
    //  20 40 60 80

    struct Node *root = newNode(50);
    root = insert(root, 30);
    root = insert(root, 20);
    root = insert(root, 40);
    root = insert(root, 70);
    root = insert(root, 60);
    root = insert(root, 80);

    // Print inorder traversal of the BST
    inorder(root);

    return 0;
}
