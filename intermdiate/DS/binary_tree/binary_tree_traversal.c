/*
 Operations On Binary Tree:
 ==========================

 1. Traversal in Binary Tree
 Traversal in Binary Tree involves visiting all the nodes of the binary tree. 
 Tree Traversal algorithms can be classified broadly into two categories,
 => DFS and BFS:

 Depth-First Search (DFS) algorithms: DFS explores as far down a branch as 
 possible before backtracking. It is implemented using recursion.
 The main traversal methods in DFS for binary trees are:

 Preorder Traversal (current-left-right): Visits the node first, then left
 subtree, then right subtree.
 Inorder Traversal (left-current-right): Visits left subtree, then the node,
 then the right subtree.
 Postorder Traversal (left-right-current): Visits left subtree, then right
 subtree, then the node.

 Breadth-First Search (BFS) algorithms: BFS explores all nodes at the present
 depth before moving on to nodes at the next depth level.
 It is typically implemented using a queue. BFS in a binary tree is commonly
 referred to as Level Order Traversal.

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../../advanced/advanced_debugging/debug.h"

struct Node *createNode(int);
void inOrderDFS(struct Node *);
void preOrderDFS(struct Node *);
void postOrderDFS(struct Node *);
void BFS(struct Node *);

struct Node{
  int data;
  struct Node *left;
  struct Node *right;
};

struct Node *createNode(int d){
  struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
  newNode->data = d;
  newNode->left = NULL;
  newNode->right = NULL;
  return newNode;
}

// In-order DFS: Left, Root, Right
void inOrderDFS(struct Node *node){
  if (node == NULL) return;
  inOrderDFS(node->left);
  printf("%d ", node->data);
  inOrderDFS(node->right);
}

// Pre-order DFS: Root, Left, Right
void preOrderDFS(struct Node *node){
  if (node == NULL) return;
  printf("%d ", node->data);
  preOrderDFS(node->left);
  preOrderDFS(node->right);
}

// Post-order DFS: Left, Right, Root
void postOrderDFS(struct Node *node){
  if (node == NULL) return;
  postOrderDFS(node->left);
  postOrderDFS(node->right);
  printf("%d ", node->data);
}

// BFS: Level order traversal
void BFS(struct Node *root){
  if (root == NULL) return;
  struct Node *queue[100];
  int front = 0, rear = 0;
  queue[rear++] = root;
  LOG(1, "queue[rear=%d] = root->data=%d", rear, root->data);

  while (front < rear) {
    LOG(1, "front=%d < rear=%d", front, rear);
    struct Node *node = queue[front++];
    LOG(1, "node = queue[front=%d]", front);
    printf("%d ", node->data);
    LOG(1, "printing node->data=%d", node->data);
    if (node->left){
      queue[rear++] = node->left;
      if(node->left->data)
        LOG(1, "queue[rear=%d] node->left->data=%d", rear, node->left->data);
    }
    if (node->right){
      queue[rear++] = node->right;
      if(node->right->data)
        LOG(1, "queue[rear=%d] node->right->data=%d", rear, node->right->data);
    }
  }
}

int main(void){
  // Creating the tree
  struct Node *root = (struct Node *)malloc(sizeof(struct Node));
  root->data = 2;
  root->left = (struct Node *)malloc(sizeof(struct Node));
  root->left->data = 3;
  root->right = (struct Node *)malloc(sizeof(struct Node));
  root->right->data = 4;
  root->left->left = (struct Node *)malloc(sizeof(struct Node));
  root->left->left->data = 5;
  root->left->right = (struct Node *)malloc(sizeof(struct Node));
  root->left->right->data = 6;
  root->right->left = (struct Node *)malloc(sizeof(struct Node));
  root->right->left->data = 7;
  root->right->right = (struct Node *)malloc(sizeof(struct Node));
  root->right->right->data = 8;

  printf("     2\n   3   4\n 5 6  7 8\n"); 
  printf("In-order DFS: ");
  inOrderDFS(root);
  printf("\nPre-order DFS: ");
  preOrderDFS(root);
  printf("\nPost-order DFS: ");
  postOrderDFS(root);
  printf("\nLevel order: \n");
  BFS(root);
  puts("");

  return 0;
}

/*
How do you perform tree traversal in a binary tree?
Tree traversal in a binary tree can be done in different ways:
In-order traversal, Pre-order traversal, Post-order traversal,
and Level-order traversal (also known as breadth-first traversal). 

*/
