/*

Case 1. Delete a Leaf Node in BST
Assign node to null.

Case 2. Delete a Node with Single Child in BST
Deleting a single child node is also simple in BST. Copy the child to the node
and delete the node.

Case 3. Delete a Node with Both Children in BST

Deleting a node with both children is not so simple. Here we have to delete the
node is such a way, that the resulting tree follows the properties of a BST.  

The trick is to find the inorder successor of the node. Copy contents of the
inorder successor to the node, and delete the inorder successor.

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../../advanced/advanced_debugging/debug.h"

struct Node {
    int key;
    struct Node* left;
    struct Node* right;
};

// Note that it is not a generic inorder successor
// function. It mainly works when the right child
// is not empty, which is  the case we need in
// BST delete.
struct Node* getSuccessor(struct Node* curr) {
    curr = curr->right;
    LOG(1, "curr = curr->right(%d)", curr->key);
    while (curr != NULL && curr->left != NULL)
        curr = curr->left;
        if(curr->left)
          LOG(1, "curr = curr->left(%d)", curr->left->key);

    LOG(1, "return curr=%d", curr->key);
    return curr;
}

// This function deletes a given key x from the
// given BST  and returns the modified root of 
// the BST (if it is modified)
struct Node* delNode(struct Node* root, int x) {
  
    // Base case
    LOG(1, "Delete key=%d", x);
    if (root == NULL)
        return root;

    // If key to be searched is in a subtree
    if (root->key > x){
        LOG(1, "search key is lower than root->key(%d) > x(%d)", root->key, x);
        root->left = delNode(root->left, x);
    }else if (root->key < x){
        LOG(1, "search key is bigger than root->key(%d) < x(%d)", root->key, x);
        root->right = delNode(root->right, x);
    }else {
        // If root matches with the given key

        // Cases when root has 0 children or 
        // only right child
        if (root->left == NULL) {
            struct Node* temp = root->right;
            free(root);
            LOG(1, "root has 0 children/only right child."
                "return temp=%p", temp);
            return temp;
        }

        // When root has only left child
        if (root->right == NULL) {
            struct Node *temp = root->left;
            free(root);
            LOG(1, "root has only left child. return temp=%p", temp);
            return temp;
        }

        // When both children are present
        LOG(1, "both children are present");
        struct Node* succ = getSuccessor(root);
        root->key = succ->key;
        LOG(1, "root->key(%d) = succ->key(%d)", root->key, succ->key);
        root->right = delNode(root->right, succ->key);
    }
    return root;
}

struct Node* createNode(int key) {
    struct Node* newNode = 
       (struct Node*)malloc(sizeof(struct Node));
    newNode->key = key;
    newNode->left = newNode->right = NULL;
    return newNode;
}

// Utility function to do inorder traversal
void inorder(struct Node* root) {
    if (root != NULL) {
        inorder(root->left);
        printf("%d ", root->key);
        inorder(root->right);
    }
}

// Driver code
int main() {
    struct Node* root = createNode(10);
    root->left = createNode(5);
    root->right = createNode(15);
    root->right->left = createNode(12);
    root->right->right = createNode(18);

    printf("      10\n");
    printf("     \/  \\ \n");
    printf("    5    15\n");
    printf("         \/ \\  \n");
    printf("        12 18\n\n");
    int x = 15;

    inorder(root);
    root = delNode(root, x);
    puts("");
    inorder(root);
    puts("");
    return 0;
}

/*
Time Complexity: O(h), where h is the height of the BST. 
Auxiliary Space: O(h).

The above recursive solution does two traversals across height when both
the children are not NULL. We can optimize the solution for this particular case
*/
