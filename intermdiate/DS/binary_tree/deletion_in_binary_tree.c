/*
4. Deletion in Binary Tree
Deleting a node from a binary tree means removing a specific node while keeping
the tree’s structure. First, we need to find the node that want to delete by
traversing through the tree using any traversal method.
Then replace the node’s value with the value of the last node in the tree
(found by traversing to the rightmost leaf), and then delete that last node.
This way, the tree structure won’t be effected.
And remember to check for special cases, like trying to delete from an
empty tree, to avoid any issues.

Note: There is no specific rule of deletion but we always make sure that during
deletion the binary tree proper should be preserved.

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../../advanced/advanced_debugging/debug.h"

struct Node *createNode(int);
struct Node *deleteNode(struct Node*, int);
void inorder(struct Node*);

struct Node {
  int data;
  struct Node *left;
  struct Node *right;
};

// Function to create a new node
struct Node *createNode(int key) {
  struct Node *newNode = 
    (struct Node*)malloc(sizeof(struct Node));
  newNode->data = key;
  newNode->left = NULL;
  newNode->right = NULL;
  return newNode;
}

// Function to delete a node from the binary tree
struct Node *deleteNode(struct Node* root, int val) {
  if (root == NULL) return NULL;

  // Use a queue to perform BFS
  struct Node *queue[100];
  int front = 0, rear = 0;
  queue[rear++] = root;
  LOG(1, "queue[rear=%d] = root->data=%d", rear, root->data);
  struct Node *target = NULL;

  // Find the target node (level order traversel)
  while (front < rear) {
    LOG(1, "front=%d < rear=%d", front, rear);

    struct Node *curr = queue[front++];
    LOG(1, "curr = queue[front=%d]", front);

    if (curr->data == val) {
      LOG(1, "curr->data(%d) == val(%d)", curr->data, val);
      target = curr;
      break;
    }
    if (curr->left) queue[rear++] = curr->left;
    if (curr->left) LOG(1, "queue[rear=%d] = curr->left=%d", rear, curr->left->data);
    if (curr->right) queue[rear++] = curr->right;
    if (curr->right) LOG(1, "queue[rear=%d] = curr->right=%d", rear, curr->right->data);
  }
  if (target == NULL) return root;
  LOG(1, "Target is found\n\n");

  // Find the deepest rightmost node and its parent
  struct Node *lastNode = NULL;
  struct Node *lastParent = NULL;
  int front1 = 0, rear1 = 0;
  struct Node *queue1[100];
  queue1[rear1++] = root;
  struct Node *parents[100];
  parents[rear1 - 1] = NULL;

  while (front1 < rear1) {
    LOG(1, "front1=%d < rear1=%d", front1, rear1);
    struct Node *curr = queue1[front1];
    LOG(1, "curr = queue1[front1=%d]", front1);
    struct Node *parent = parents[front1++];
    LOG(1, "parent = parents[front1=%d]", front1);

    lastNode = curr;
    lastParent = parent;
    if (curr)
     LOG(1, "lastNode = curr(%d)", curr->data);
    if (parent == NULL)
      LOG(1, "lastParent = parent(%p)", NULL);
    if (parent)
      LOG(1, "lastParent = parent(%d)", parent->data);

    if (curr->left) {
      queue1[rear1] = curr->left;
        LOG(1, "queue1[rear1=%d] = curr->left(%d)", rear1, curr->left->data);
      parents[rear1++] = curr;
        LOG(1, "parents[rear1++=%d] = curr(%d)", rear1, curr->data);
      
    }
    if (curr->right) {
      queue1[rear1] = curr->right;
      if(curr->right)
        LOG(1, "queue1[rear1=%d] = curr->right(%d)", rear1, curr->right->data);
      parents[rear1++] = curr;
        LOG(1, "parents[rear1++=%d] = curr(%d)", rear1, curr->data);
    }
  }

  // Replace target's value with the last node's value
  target->data = lastNode->data;

  // Remove the last node
  if (lastParent) {
    if (lastParent->left == lastNode) lastParent->left = NULL;
    else lastParent->right = NULL;
    free(lastNode);
  } else {
    free(lastNode);
    return NULL;
  }

  return root;
}

// Inorder: left, root, right
void inorder(struct Node* root) {
  if (root == NULL) return;
  inorder(root->left);
  printf("%d ", root->data);
  inorder(root->right);
}

int main(void) {
  struct Node *root = createNode(2);
  root->left = createNode(3);
  root->right = createNode(4);
  root->left->left = createNode(5);
  root->left->right = createNode(6);

  printf("Original tree (in-order): ");
  inorder(root);
  printf("\n");

  int valToDel = 3;
  root = deleteNode(root, valToDel);

  printf("Tree after deleting %d (in-order): ", valToDel);
  inorder(root);
  printf("\n");

  return 0;
}

