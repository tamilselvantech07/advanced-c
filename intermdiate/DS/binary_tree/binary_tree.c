#include <stdio.h>
#include <stdlib.h>

/*
 A binary tree is a non-linear data structure consisting of nodes, where
 each node has at most two children (left child and the right child).

 Types of binary trees:
 Binary trees can be classified into various types, including full binary 
 trees, complete binary trees, perfect binary trees, balanced binary trees
 (such as AVL trees and Red-Black trees), and degenerate (or pathological) 
 binary trees.

 Terminologies in Binary Tree:
 -----------------------------
 Nodes: The fundamental part of a binary tree, where each node contains data 
   and link to two child nodes.

 Root: The topmost node in a tree is known as the root node. It has no parent
   and serves as the starting point for all nodes in the tree.

 Parent Node: A node that has one or more child nodes. In a binary tree,
   each node can have at most two children.

 Child Node: A node that is a descendant of another node (its parent).

 Leaf Node: A node that does not have any children.

 Internal Node: A node that has at least one child. This includes all nodes 
   except the root and the leaf nodes.

 Depth of a Binary Tree: The number of edges from a specific node to the 
   root node. The depth of the root node is zero.

 Height of a Binary Tree: The number of nodes from the deepest leaf node to
   the root node.


                     Root  Key
                       |   /
                       |  /
                        A                  level 0  }
                       / \                          |
                      /   \                         |
      Parent Node -> B     C ->B,C sibling level 1  | Height of tree
                    / \   / \                       |
     Child Node -> D   E  F  G             level 2  }
                   ^   ^  ^  ^
                   Leaf Nodes


 Properties of Binary Tree:
 --------------------------
 The maximum number of nodes at level L of a binary tree is 2(pow L)
 The maximum number of nodes in a binary tree of height H is 2(pow H) – 1
 Total number of leaf nodes in a binary tree = total number of nodes with
   2 children + 1
 In a Binary Tree with N nodes, the minimum possible height or the minimum
   number of levels is Log2(N+1)
 A Binary Tree with L leaves has at least | Log2L |+ 1 levels
*/


struct Node {
  int data;
  struct Node *left;
  struct Node *right;
};

struct Node* createNode(int data) {
  struct Node* newNode =
    (struct Node*)malloc(sizeof(struct Node));
  newNode->data = data;
  newNode->left = NULL;
  newNode->right = NULL;
  return newNode;
}

int main(void){
  // Initialize and allocate memory for tree nodes
  struct Node *firstNode = createNode(2);
  struct Node *secondNode = createNode(3);
  struct Node *thirdNode = createNode(4);
  struct Node *fourthNode = createNode(5);

  // Connect binary tree nodes
  firstNode->left = secondNode;
  firstNode->right = thirdNode;
  secondNode->left = fourthNode;

  return 0;
}


/*
Types of Binary Tree
Binary Tree can be classified into multiples types based on multiple factors:

On the basis of Number of Children:
-----------------------------------
Full Binary Tree
Degenerate Binary Tree
Skewed Binary Trees

On the basis of Completion of Levels:
-------------------------------------
Complete Binary Tree
Perfect Binary Tree
Balanced Binary Tree(AVL, Red Black Tree)

On the basis of Node Values:
----------------------------
Binary Search Tree
AVL Tree
Red Black Tree
B Tree
B+ Tree
Segment Tree

*/

/*
 * What is the difference between a Binary Tree and a Binary Search Tree?

 A binary tree is a hierarchical data structure where each node has at most
 two children, whereas a binary search tree is a type of binary tree in which
 the left child of a node contains values less than the node’s value, and the
 right child contains values greater than the node’s value.

 *What is a balanced binary tree?
 
 A balanced binary tree is a binary tree in which the height of the left and 
 right subtrees of every node differ by at most one.
 Balanced trees help maintain efficient operations such as searching,
 insertion, and deletion with time complexities close to O(log n).

*/

/*
 Advantages of Binary Tree:
 --------------------------
 Efficient Search: Binary Search Trees (a variation of Binary Tree) are
 efficient when searching for a specific element, as each node has at most
 two child nodes when compared to linked list and arrays
 
 Memory Efficient: Binary trees require lesser memory as compared to other 
 tree data structures, therefore they are memory-efficient.
 
 Binary trees are relatively easy to implement and understand as each node
 has at most two children, left child and right child.

 Disadvantages of Binary Tree
 ----------------------------
 Limited structure: Binary trees are limited to two child nodes per node, 
 which can limit their usefulness in certain applications.
 For example, if a tree requires more than two child nodes per node,
 a different tree structure may be more suitable.
 
 Unbalanced trees: Unbalanced binary trees, where one subtree is significantly
 larger than the other, can lead to inefficient search operations.
 This can occur if the tree is not properly balanced or if data is inserted in
 a non-random order.

 Space inefficiency: Binary trees can be space inefficient when compared to
 other data structures. This is because each node requires two child pointers,
 which can be a significant amount of memory overhead for large trees.

 Slow performance in worst-case scenarios: In the worst-case scenario, 
 a binary tree can become degenerate or skewed, meaning that each node has only
 one child. In this case, search operations can degrade to O(n) time
 complexity, where n is the number of nodes in the tree.

 
 Applications of Binary Tree
 ---------------------------
 Binary Tree can be used to represent hierarchical data.
 
 Huffman Coding trees are used in data compression algorithms.
 
 Priority Queue is another application of binary tree that is used for 
 searching maximum or minimum in O(1) time complexity.
 
 Useful for indexing segmented at the database is useful in storing cache
 in the system,
 
 Binary trees can be used to implement decision trees, a type of machine
 learning algorithm used for classification and regression analysis.

*/
