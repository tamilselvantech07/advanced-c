#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node_
{
	int value;
	struct node_ *next;
}NODE;

void print_node(NODE *m1)
{
	NODE *curr = m1;
	printf("Linked List: ");
	while(curr != NULL)
	{
		printf("%d ", curr->value);
		curr = curr->next;
	}
  printf("\n");
}

void remove_an_item(NODE **node_ptr, int *value)
{
  NODE *curr = *node_ptr;
	NODE *prev = NULL;

	printf("%s\n", __func__);
  if(curr != NULL && (!(memcmp(&curr->value, value, sizeof(int)))))
	{
		printf("Head matches\n");
		*node_ptr = curr->next;
	  printf("Removed curr->value:%d\n", curr->value);
		free(curr);
		goto exit;
	}

	while(curr != NULL && memcmp(&curr->value, value, sizeof(int)))
	{
		prev = curr;
		curr = curr->next;
		if(curr != NULL)
		  printf("prev->value:%d and curr->value:%d\n", prev->value, curr->value);
	}

	if(curr == NULL)
	{
		printf("curr is NULL\n");
		goto exit;
	}

  prev->next = curr->next;
	printf("Removed curr->value:%d\n", curr->value);
	free(curr);

exit:
  return;
}


int main(void)
{
  NODE *m1, *m2, *m3, *m4, *m5, *m6, *m7, *m8, *m9;

	m1 = malloc(sizeof(NODE));
	m2 = malloc(sizeof(NODE));
	m3 = malloc(sizeof(NODE));
	m4 = malloc(sizeof(NODE));
	m5 = malloc(sizeof(NODE));
	m6 = malloc(sizeof(NODE));
	m7 = malloc(sizeof(NODE));
	m8 = malloc(sizeof(NODE));
	m9 = malloc(sizeof(NODE));

  m1->value = 1;
	m1->next = m2;
	m2->value = 3;
	m2->next = m3;
	m3->value = 5;
	m3->next = m4;
	m4->value = 7;
	m4->next = m5;
	m5->value = 7;
	m5->next = m6;
	m6->value = 9;
	m6->next = m7;
	m7->value = 11;
	m7->next = m8;
	m8->value = 13;
	m8->next = m9;
	m9->value = 15;
	m9->next = NULL;

	print_node(m1);
	int item;
	int choice;
	do
	{
		printf("Enter item to be removed:");
		scanf("%d", &item);
		remove_an_item(&m1, &item);
		print_node(m1);
		printf("Do you want to remove again(1/0):");
		scanf("%d", &choice);
	}while(choice == 1);

// Note: There is an bug with item, and while loop input.
// i.e, when you give any chars like 'ex' after one loop,
// it will loop indefinitely.
	return 0;
}
