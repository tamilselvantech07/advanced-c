#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node_
{
	int value;
	struct node_ *next;
}NODE;

/* Given a reference (pointer to pointer) to the head of a list
and an int, inserts a new node on the front of the list.
*/
void push(NODE **head_ref, int new_data)
{
	/* 1. allocate node */
	NODE *new_node	= malloc(sizeof(NODE));

	/* 2. put in the data */
	new_node->value = new_data;

	/* 3. Make next of new node as head */
	new_node->next = (*head_ref);

	/* 4. move the head to point to the new node */
	(*head_ref) = new_node;
}

/* Given a reference (pointer to pointer) to the head
of a list and an int, appends a new node at the end */
void append(NODE **head_ref, int new_data)
{
	/* 1. allocate node */
	NODE *new_node	= malloc(sizeof(NODE));

	NODE *last = *head_ref; /* used in step 5*/

	/* 2. put in the data */
	new_node->value = new_data;

	/* 3. This new node is going to be the last node, so
	make next of it as NULL*/
	new_node->next = NULL;

	/* 4. If the Linked List is empty, then make the new
	* node as head */
	if (*head_ref == NULL) {
		*head_ref = new_node;
		return;
	}

	/* 5. Else traverse till the last node */
	while (last->next != NULL)
		last = last->next;

	/* 6. Change the next of last node */
	last->next = new_node;
	return;
}


void print_node(NODE *head)
{
  while (head) 
  {
    printf("[%i]\t[%p]->%p\n", head->value, head, head->next);
    head = head->next;
  }
  printf("\n\n");
}

void remove_an_item(NODE **node_ptr, int *value)
{
  NODE *curr = *node_ptr;
	NODE *prev = NULL;

	printf("%s\n", __func__);
  if(curr != NULL && (!(memcmp(&curr->value, value, sizeof(int)))))
	{
		printf("Head matches\n");
		*node_ptr = curr->next;
	  printf("Removed curr->value:%d\n", curr->value);
		free(curr);
		goto exit;
	}

	while(curr != NULL && memcmp(&curr->value, value, sizeof(int)))
	{
		prev = curr;
		curr = curr->next;
		if(curr != NULL)
		  printf("prev->value:%d and curr->value:%d\n", prev->value, curr->value);
	}

	if(curr == NULL)
	{
		printf("curr is NULL\n");
		goto exit;
	}

  prev->next = curr->next;
	printf("Removed curr->value:%d\n", curr->value);
	free(curr);

exit:
  return;
}


int main(void)
{
  NODE *list = malloc(sizeof(NODE));
  list->next = NULL;
  push(&list, 2);
  push(&list, 1);
  append(&list, 3);
	append(&list, 5);
  append(&list, 7);
  append(&list, 9);
	append(&list, 11);
  append(&list, 13);
  append(&list, 15);

	print_node(list);
	int item;
	int choice;
	do
	{
		printf("Enter item to be removed:");
		scanf("%d", &item);
		remove_an_item(&list, &item);
		print_node(list);
		printf("Do you want to remove again(1/0):");
		scanf("%d", &choice);
	}while(choice == 1);

// Note: There is an bug with item, and while loop input.
// i.e, when you give any chars like 'ex' after one loop,
// it will loop indefinitely.
	return 0;
}
