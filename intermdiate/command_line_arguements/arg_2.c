// C program to illustrate
// command line arguments
#include <stdio.h>
#include <stdlib.h>
 
int main(int argc, char* argv[])
{
    printf("Program name is: %s\n", argv[0]);
 
    if (argc == 1)
        puts("No Extra Command Line Argument Passed "
               "Other Than Program Name\n");
 
    if (argc >= 2) {
        printf("Number Of Arguments Passed: %d\n", argc);
        puts("----Following Are The Command Line "
               "Arguments Passed----\n");
	int a[argc];
        for (int i = 1; i < argc; i++) {
            printf("argv[%d]: %s\n", i, argv[i]);
	    a[i] = atoi(argv[i]);
	    printf("a[%d]=%d\n", i, a[i]);
	}
    }
    
    return 0;
}
