#include<stdio.h>
#include<stdlib.h>


int main()
{

	int a[10];  // static memory allocation for array;

	int *b;     // if we need to allocate an array dynamically, define pointer var.
	int length = 0;

	printf("Enter a lenght: ");
	scanf("%d", &length);

	b = malloc(length * sizeof(int));

	for(int i=0; i<length; i++)
	 b[i]=i;

	for(int i=0; i<length; i++)
		printf("b[%d]= %d\n", i, b[i]);

	free(b);

	return 0;
}
