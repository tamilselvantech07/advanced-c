#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<unistd.h>  // sleep();


int main(void)
{
	time_t now = time(NULL);

	//number of secs since Jan 1, 1970
	printf("%lu\n", now);

	sleep(2);
	time_t new_t;
	time(&new_t);
	printf("difftime: %f\n", difftime(new_t, now));

	char *c_time = ctime(&now);
	printf("%s", c_time);


	struct tm *local_time = localtime(&now); // Local Time Zone

	printf("tm_sec=%d\n", local_time->tm_sec);
	printf("tm_min=%d\n", local_time->tm_min);
	printf("tm_hour=%d\n", local_time->tm_hour);
	printf("tm_mday=%d\n", local_time->tm_mday);
	printf("tm_mon=%d\n", local_time->tm_mon);
	printf("tm_year=%d\n", local_time->tm_year);
	printf("tm_wday=%d\n", local_time->tm_wday);
	printf("tm_yday=%d\n", local_time->tm_yday);
	printf("tm_isdst=%d\n", local_time->tm_isdst);

	char *asc_local_time = asctime(local_time);
	printf("asctime(localtime): %s", asc_local_time);

	// Linux "date" cmd will return in the below format
	char s[100];
	strftime(s, 100, "%A %d %B %Y %H:%M:%S %p %Z", local_time);
	// %A Full Weekname, %a Tue 
	// %B Full month name,%b Jan, %m month(01-12)
	// %d mday(1-31), %H 24hour, %l 12h %M minutes
	// %S seconds, %p (AM/PM), %Z timezone
	// %x date rep(09/19/24), %X time rep 02:50:57
	printf("%s\n", s);

	printf("\nUTC Time\n");

	struct tm *gm_time = gmtime(&now); // Greenwich Mean Time (UTC) Zone
	printf("tm_sec=%d\n", gm_time->tm_sec);
	printf("tm_min=%d\n", gm_time->tm_min);
	printf("tm_hour=%d\n", gm_time->tm_hour);
	printf("tm_mday=%d\n", gm_time->tm_mday);
	printf("tm_mon=%d\n", gm_time->tm_mon);
	printf("tm_year=%d\n", gm_time->tm_year);
	printf("tm_wday=%d\n", gm_time->tm_wday);
	printf("tm_yday=%d\n", gm_time->tm_yday);
	printf("tm_isdst=%d\n", gm_time->tm_isdst);

	char *asc_gm_time = asctime(gm_time);
	printf("asctime(UTC-gmtime): %s", asc_gm_time);

	local_time->tm_sec = local_time->tm_sec + 1;
	time_t new_time = mktime(local_time);  // converts struct tm to time_t value

	printf("now     : %ld\n", now);
	printf("new_time: %ld\n", new_time);


}
