#include <stdio.h>
#include "supplementary.c"

/* =======================================================
Notes: anytime when the storage class is not specified for a function it means it is 'extern' so it can be used and seen from other files as well
========================================================== */

// extern void foo(); // we got compilation error becoz it is static fn. the only way to call the fn is header inclusion


int main(int argc, char *argv[])
{
    foo();
    bar();

    return 0;
}


/*
Note:  If you're including the header file of the function declared file, it won't throw error, even you're
using extern keyword for static function which defined in another file.

static and structures:
======================
static variables should not be declared inside a structure.
the C compiler requires the entire structure elements to be placed together. memory allocation for
structure members should be contiguous.
It is possible to declare a structure
  Inside a function(stack segment)
  Allocate memory dynamically(heap segment)
  It can be even global.
Whatever might be the case, all structure members should reside in the same memory segment.
It is possible to have an entire structure as static.
*/