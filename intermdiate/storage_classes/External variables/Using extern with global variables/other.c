extern int myVariable; //this informs us that this variable is defined elsewhere as a global variable and can be accessed and modified from here
extern char myName[]; //this informs us that this variable is defined elsewhere as a global variable and can be accessed and modified from here
                      // Note: Don't mention size of the single dimension char array when using extern keywork.
					  // But you can mention 2D array size when using extern keyword.  i.e, extern char myName[][10];
static void foo(void)
{
	myVariable++;
}
