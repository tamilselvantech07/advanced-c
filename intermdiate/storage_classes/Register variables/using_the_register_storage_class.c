#include <stdio.h>

/*==============================================================================
Note: The register storage class is used with frequently accessed variables to
store these variables in a CPU register, unlike the normal way when the 
variables are stored in the RAM.
It gains speed, but the size of the variable gets limited to the size of
the CPU register.
Additionally there can not be applied the operator & address of to a variable
of register type
==============================================================================*/


int main(void)
{
    register int counter;

    for(counter=0; counter<=10000;counter++)
    {
        printf("The value of the counter is %d.\n",counter);
    }


    return 0;
}


/* Note:
A process register(CPU register) is one of a small set of data holding places 
that are part of the computer processor.
A register may hold an instruction, a storage address, or any kind of data.
The register storage class is used to define the local variables that should
be stored in register instead of RAM(memory).

You cannot obtain the address of a register variable using pointers.
 cannot have the unary '&' operator applied to it,(as it does not have memory
 location)

register int x;
int* a=&x;   // Throws error.

cannot use static keyword with register. And also register is not used outside
of the main() scope.
*/
