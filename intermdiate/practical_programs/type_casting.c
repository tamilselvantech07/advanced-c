#include<stdio.h>

int main()
{
	int a=60, b=17;
	float res;

	res = a/b;
	printf("Without typecasting a/b=%.3f\n",res);

	res=(float)a/b;
	printf("With typecasting a/b=%.3f\n",res);

	return 0;

}
