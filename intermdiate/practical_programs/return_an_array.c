#include<stdio.h>
#include<stdlib.h>


/* static array
void set_array(int value, int *array)
{

	for(int i=0; i<5; i++)
		array[i] = value;
}
*/

int *set_array(int value)
{
	int *array = malloc(sizeof(int) * 5);

  for(int i=0; i<5; i++)
    array[i] = value;
	
	return array;
}

int main()
{
	int local_array[] = {1,2,3,4,5};

	int *result_ = local_array;

	printf("result[3]: %d\n", result_[3]);

	printf("array: %p\n", local_array);
	printf("result: %p\n", result_);

	// like this we can get an array by its pointer

  /* static array
  int array[5];
	set_array(4, array);
	for(int j=0; j<5; j++)
	{
		printf("array[%d] = %d\n", j, array[j]);
	}
  */

	// dynamic memory
	int *result = set_array(4);
	for(int j=0; j<5; j++)
	{
		printf("result[%d] = %d\n", j, result[j]);
	}

	free(result);

  return 0;
}
