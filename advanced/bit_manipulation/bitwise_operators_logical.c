#include<stdio.h>

/********************************************************************************
 *computer programming tasks that require bit manipulation include
 *  > low-level device control
 *  > error detection
 *  > correction algorithms
 *  > data compression
 *  > encryption algorthms
 *  > optimization
 *
 * a bitwise operation operates on one or more binary numbers at the level of 
 * their individual bits
 *  > used to manipulate values for comparisons and calculations
 *  > substantially faster than division, several times faster than multiplication
 *    and sometimes significantly faster than addition.
 *
 * C offers bitwise logical operators and shift operators
 *  > operate on the bits in integer values
 *    & Binary AND - copies a it to the result if exists in both operands
 *    | Binary OR - copies a bit if it exist in either operand 
 *    ^ Binary XOR copies the bit if it is set in one operand but not both 
 *    ~ Binary Ones Complement Operator is unary and has the effect
 *      of 'flipping' bits
 *
 *
 *   they operate on each bit indepenently of the bit to the left or right
 *    > do not confuse them with regular logical operators(&&, || and !)
 *
 *   all the logical operators listed in the above (execpt ones comoplement
 *   operator ~) are binary operator .  Takes two operands.
 *
 *
 * Use Case
 * bit operations can be performed on any type of integer value in C
 *  > int, short, long, long long, and signed or unsigned
 *  > and on characters, but cannot be performed on "Floating-point" values
 *
 * a bit mask is data that is used for bitwise operations
 *  > using a mask, multiple bits in a Byte can be set either on, off or 
 *  inverted from on to off(or vice versa) in a signle bitwise operation
 *
 *  one major use of the bitwise AND and bitwise OR is in operations to test 
 *  and set individual its in an integer variable
 *   > can use individual bits to store data that involve one of two choices
 *
 * you could use a single integer variable to store several charecter of a 
 * person
 *  > store whether the person is male or female with one bit
 *  > use thress other bits to specify whether the person can speack French,
 *    German or Italian.
 *******************************************************************************/


 int main()
 {
   short int w1 = 25;
   short int w2 = 77;
   short int w3 = 0;

   w3 = w1 & w2;

   // 0000000 00011001
   // 0000000 01001101
   // ----------------
   // 0000000 00001001 = 9
  
  printf("w1 & w2 = %d\n", w3);
  
  w3 = w1 | w2;
  printf("w1 | w2 = %d\n", w3);

  w3 = w1 ^ w2;
  printf("w1 ^ w2 = %d\n", w3);

// One of the best example is swapping numbers
//
// temp = w1;
// w1 = w2;
// w2 = temp;
// instead of this, you can use XOR operator to swap two numbers.

  printf("before swapping: w1 = %d and w2 = %d\n", w1, w2);
  w1 ^= w2;
  w2 ^= w1;
  w1 ^= w2;
  printf("after swapping: w1 = %d and w2 = %d\n", w1, w2);

  signed int w4 = 2;
  signed int result = 0;

  result = ~(w4);  // -3
  printf("w4 = %d\n~(w4) = %d\n", w4, result);

  // 0000 0010  == 2
  // ---------
  // 1111 1101  = 253
  //
  // 0000 0011   To convert +ve 3
  // 1111 1100
  //
  // 1111 1101
  

  signed int w5 = 154;
  result = ~(w5);  // -155
  printf("w5 = %d\n ~(w5) = %d\n", w5, result);
  
  // 1001 1010 = 154
  // ---------
  // 0110 0101 = twos compliment = -155
  //
  // 1001 1011 = 155 // Flip the bytes
  // 0110 0100 + 1 = 155,  0110 0101

  // Ones compliment operator: ~
  // useful when you do not know the precise bit size of the quantity that you
  // are dealing with in an operation
  //  > can help make a program more portable
  //
  // to set the low-order bit of an int called w1 to 0, you can AND w1 with an
  // int consisting of all 1s except for a single 0 in the rightmose bit
  // w1 &= 0xFFFFFFFE;
  //
  // works fine on machines in which an integer is represented by 32 bits
  // if you replace the preceding statement with
  //
  // w1 &= ~1;
  // w1 gets ANDed with the correct value on any machine because the ones 
  // complement of 1 is calculated and consists of as many leftmost bits as
  // are necessary to fill the size of an int(31 leftmost bits on a 32-bit
  // integer system)

  return 0;
 }
