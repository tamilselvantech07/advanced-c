/*
 * we understand that you can perform all sorts of sophisticated operations
 * on bits
 *  often performed on date items that contain packed information
 *
 * you can pack information into the bits of a byte if you do not need to
 * use the entire byte to represent the data
 *   flags that are used for a boolean true or false condition can be 
 *   represented in a single bit on a computer
 *
 * two methods are available in C that can be used to pack information
 * together to make better use of memory
 *  bit fields and bitwise operators
 *
 * you could use an unsigned int/long variable to hold the same information
 * OR you could use a structure the same size as unsigned int to hold state
 * information
 *
 * we will discuss the first option
 *  represent the data inside a normal int and then access the desired bits
 *  of the int using the bit operations with a bitmask
 *  this is a bit more awkward to do(than bit fields)
 *
 * Pack Information into an int/long variable:
 * -------------------------------------------
 * if you need to store many flags inside a large table, the amount of
 * memory that is wasted could become significant
 *
 * an int or long can be used to conserve memory space
 *
 * flags that are used for a boolean true or false condition can be
 * represented in a single bit on a computer
 *  each bit in the int can be set to 1(true) or 0(false)
 *
 * 1011 1001
 *
 * we can access the desired bits of the int using the bit operators 
 * provided by C
 *  first bit is true, second bit is false, third bit is true(each bit
 *  represents a flag)
 *  we are essentially stroing eight different values in a single int
 *
 *
 * Example:
 * suppose you want to pack five data values into a word because you have
 * to maintain a very large table of these values in memory
 *  assume that three of these data values are flags (f1, f2, and f3)
 *  the fourth value is an integer called type, which ranges from 1 to 255
 *  the final value is an integer called index, ranges from 0 to 100,000
 *
 * stroing the values of the flags f1, f2 and f3 only requires three bits
 * of storage
 *  one bit for the true/false value of each flag
 *
 * storing the value of the int type required eight bits of storage
 *
 * storing the value of the integer index requies 18 bits
 *
 * the total amount of storage needed to store the five data values is 29 bits
 *
 * you could define an integer variable that could be used to contain all
 * five of these values
 *
 * Example (cont'd)
 *
 * unsigned int packed_data; // 32 bits on most systems
 *
 * you can then arbitrarily assing specific bits or fields inside
 * packed_data to be used to store the five data values
 *  packed_data has three unused bits
 * 
 *  unused  f1 f2 f3 type      index
 *   | | |   |  |  |   |         | 
 *   0 0 0   0  0  0 00000000 000000000000000000   
 *  
 *
 * Example, setting bits:
 * ----------------------
 * you can apply the correct sequence of bit operations to packed_data to 
 * set and retrieve values to and from the fields of the int.
 *
 * to set the type field of packed_data to 7
 *   shift the value 7 to the left the appropriate number of places and
 *   then OR it into packed_data
 *
 * packed_data |= 7 << 18;
