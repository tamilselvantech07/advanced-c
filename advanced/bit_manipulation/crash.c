#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
  printf("Program started");

  // allocating memory to p
  int* p = (int*)malloc(sizeof(int));

  *p = 100;

  // deallocated the space allocated to p
  free(p);

  //  segmentation fault
  //  as now this statement is illegal
  *p = 110;

  // Stack over flow; segmentation fault
  int array[2000000000];

  printf("Program Exited successfully %d", 10/0);
}