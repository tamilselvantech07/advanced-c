#include<stdio.h>
#include<math.h>
#include "../advanced_debugging/debug.h"

int convertBinaryToDecimal(long long n);
long long convertDecimalToBinary(int n);

int main()
{
  long long n;
  int result;
  printf("Enter a binary number: ");
  scanf("%lld", &n);
  result = convertBinaryToDecimal(n);
  printf("%lld in binary = %d in decimal \n", n, result);


  int m;
  long long res;
  printf("Enter a decimal number: ");
  scanf("%d", &m);
  res = convertDecimalToBinary(m);
  printf("%d in decimal = %lld in binary\n", m, res);

  return 0;
}

int convertBinaryToDecimal(long long n)
{
  int decimalNumber = 0, i = 0, remainder = 0;

  while(n != 0)
  {
    remainder = n % 10;
    LOG(1, "remainder = n(%lld) mod 10 = %d\n", n , remainder);

    n = n / 10;
    LOG(2, "n = n / 10 == %lld\n", n); 

    LOG(2, "current decimalNumber =  %d\n", decimalNumber);
    decimalNumber += remainder*pow(2, i);
    LOG(1, "decimalNumber+=  remainder(%d) * pow(2, i(%d)) == %d\n\n",
        remainder, i, decimalNumber);
    ++i;
  }

  return decimalNumber;
}

long long convertDecimalToBinary(int n)
{
  long long binaryNumber = 0;
  int i = 1, remainder;

  while(n !=0)
  {
    remainder = n%2;
    LOG(1, "remainder = n(%d) mod 2 = %d\n", n , remainder);

    n = n / 2;
    LOG(2, "n = n / 2 == %d\n", n); 

    LOG(2, "current binaryNumber =  %lld\n", binaryNumber);
    binaryNumber += remainder * i;
    LOG(1, "binaryNumber+=  remainder(%d) * i(%d) == %lld\n\n",
        remainder, i, binaryNumber);

    i = i * 10;
  }

  return binaryNumber;
}