#include <stdio.h>

/******************************************************************************
 *
 * Two methods are available in C that can be used to pack information together
 * to make better use of memory.
 *   1. bit fields
 *   2. bitwise operators
 *
 * We will discuss about the second option in the lecture.
 *  you can use bit fields in a structure to address invidual bits or groups of
 *  bits in a value.
 *  details are implementation independent.
 *
 *
 * Bit Fields:
 *  a bit field allows you to specify the number of bits in which an int member
 *  structure is stored.
 *    uses a special syntax in the structure definition that allows you to
 *    define a field of bits and assign a name to that field.
 *    should use the explicit declarations signe int or unsigned int to avoid
 *    problems with hardware dependencies.
 *      C99 and C11 additionally allow type _Bool bit fields.
 *
 *  bit fields enable better memory utilization by storing data in the minimum
 *  nuber of bits required.
 *    format enables you to allocate a specified number of bits for a data item.
 *    can easily set and retrice its value without having to use masking and
 *    shifting.
