#include<stdio.h>

/*******************************************************************************
 * C also has left-shift(<<) and right-shift(>>) operators
 *   > each produes a value formed by shifting the bits in a pattern the
 *   indicated number of bits to the lefr or right
 *
 * for the left-shift operator,the vacated bit are set to 0(vs for right).
 *
 * Undefined Results:
 *  if you shift a value to the left or right by an amount that is greater than
 *  or equal to the number of bits in the size of the data item you will get
 *	a undefined result.
 *    On a machine that represents interger in 32-bit.
 *  		 shitfing an integer to the right or left by 32 or more bits is not
 *			 guaranteed to produce  a defined result in your program.
 ******************************************************************************/

 int main()
 {                   // <-high-order bitss     low order bits->       
	 int w1 = 138;     // 0000 0000 0000 0000 0000 0000 1000 1010
	 int w2 = 138;    
	 int result = 0;

	 result = w1 << 2; // 0000 0000 0000 0000 0000 0010 0010 1000
	 print("%d", result);

	 result = w2 >> 2; // 0000 0000 0000 0000 0000 0000 0010 0010
	 print("%d", result);

   
	 return 0;
 }
