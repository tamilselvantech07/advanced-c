#include <stdio.h>

// Write a C program to input any number from a user
//   the program should check whether nth bit of the given is set(1) or not
//   the program should set nth bit of the given number as 1
//
// 
// Enter any number: 10
// Enter nth bit to check and set(0-31): 2
// The 2 bit is set to 0
//
//  
//      0000 1010   num=10 (before setting the 2nd bit)
//
// >> 2 0000 0010 10 (right shifted 2)
// &1   0000 0001    (mask) 
//      ---------
// bitS 0000 0000
//
//
//     0000 0001   one(mask)
//  00 0000 0100   shifted 2 bytes of one and perfrom OR with num
//   | 0000 1010   num=10
//     ---------
//     0000 1110   newNum=14
//

int main() {
    int num, position, newNum, bitStatus;

    /* Input number from user */
    printf("Enter any number: ");
    scanf("%d", &num);

    /* Input bit position you want to set */
    printf("Enter nth bit to check and set (0-31): ");
    scanf("%d", &position);

    /* Right shift num, position times and perform bitwise AND with 1 */
    bitStatus = (num >> position) & 1;
    printf("The %d bit is set to %d\n", position, bitStatus);

    /* Left shift 1, n times and perform bitwise OR with num */
    newNum = (1 << position) | num;
    printf("\nBit set successfully.\n\n");

    printf("Number before setting %d bit: %d (in decimal)\n", position, num);
    printf("Number after setting %d bit: %d (in decimal)\n", position, newNum);

    return 0;
}
