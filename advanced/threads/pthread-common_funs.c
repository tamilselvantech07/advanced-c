#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>

#if 0
void *calls(void *ptr)
{
	// using pthread_self() get current thread id
	printf("In function \nthread id = %lu\n", pthread_self());
	pthread_exit(NULL);
	return NULL;
}

int main()
{
	pthread_t thread;
	pthread_create(&thread, NULL, calls, NULL);
	printf("In main \nthread id = %lu\n", thread);
	pthread_join(thread, NULL);

	return 0;
}
#endif

#if 0
void *threadFn(void *arg)
{
	pthread_detach(pthread_self());
	sleep(1);
	printf("Thread Fn\n");
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t tid;
	int ret = pthread_create(&tid, NULL, threadFn, NULL);

	if(ret != 0)
	{
		perror("Thread Creation Failed\n");
		exit(1);
	}

	printf("after thread created in Main\n");
	pthread_exit(NULL);

	return 0;
}
#endif

#if 0
/******************************************************************************
Stack Management:
=================
the POSIX standard does not dictate the size of a thread's stack
 > implementation dependent and vaires

exceeding the default stack limit is often very easy to do
 > results in program termination and/or corrupted data

safe and protable programs do not depend upon the default stack limit
 > instead, explicitly allocate enough stack for each thread by using the
   pthreaddd_attr_setstacksize(attr, stacksize) function.
*******************************************************************************/

pthread_attr_t attr;

void *dowork(void *threadid)
{
	long tid;
	size_t mystacksize;

	tid = (long)threadid;
	pthread_attr_getstacksize(&attr, &mystacksize);

	printf("Thread %ld: stack size = %li bytes\n", tid, mystacksize);

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t myThread;
	size_t stacksize;
	pthread_t myID;
	long t = 5;

	pthread_attr_init(&attr);
	pthread_attr_getstacksize(&attr, &stacksize);
	printf("Default stack size = %li\n", stacksize);
	stacksize = 9000000;
	printf("Amount of stack needed per thread = %li\n", stacksize);
	pthread_attr_setstacksize(&attr, stacksize);

	printf("Creating thread with stack size = %li bytes\n", stacksize);

	myID = pthread_create(&myThread, &attr, dowork, (void *)t);

	if(myID)
	{
		printf("ERROR: return code from pthread_create() is &lud\n");
		exit(-1);
	}

	pthread_exit(NULL);
}

#endif

/******************************************************************************
pthread_equal and pthread_once

the pthread_equal function compares two thread IDs, return 0 if two IDs are 
different, otherwise a non-zero value is returned.
 pthread_equal(thread1, thread2);

the pthread_once function executes the init_routine exactly once in a process
 > any  subsequent call will have no effect.
 pthread_once(once_control, init_routine);

the init_routine is typically an initialization routine
the once_control parameter is a synchronization control structure that
requires initialization prior to calling pthread_once
  > pthread_once_t once_control = PTHREAD_ONCE_INIT;

*******************************************************************************/

int pthread_cancel(pthread_t thread);

void *my_function(void *ptr)
{
	printf("Hello World!\n");
	pthread_cancel(pthread_self());
	return NULL;
}

int main()
{
	pthread_t thread = NULL;
	pthread_create(&thread, NULL, my_function, NULL);

	pthread_join(thread, NULL);

	return 0;
}

