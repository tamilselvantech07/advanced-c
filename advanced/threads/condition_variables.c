/******************************************************************
 * condition varibles provide yet another way for threads to
 * synchronize
 *
 * while mutexex implement synchronization by controlling thread
 * access to data, condition variables allow threads to synchronize
 * based upon the actual value of data
 *
 * without condition variables, the programmer would need to have
 * threads continually polling(possibly in a critical section), to
 * check if a condition is met,
 *  > can be very resource consuming since the thread would be
 *   continuously busy in this activity
 *
 * a condition variable is a way to achieve the same goal without
 * polling
 *
 * the condition variable mechanism allows threads to suspend
 * execution and relinquish the processor until some condition is
 * true
 *
 * a condition variable must always be associated with a mutex
 *   > to avoid deadlocks created by one thread preparing to wait
 *    and another thread which may signal the condition before the
 *    first thread actually waits on it
 *   > the thread will be prepetaully waiting for a signal that is
 *    never sent
 *
 * any mutex can be used with a condition variable
 *   > there is no explicit link between the mutex and the condition
 *    variable
 *
 * you can think of a condition variable as a "big pillow" in the
 * sense that threads can fail asleep on a condition variable and be
 * woken from that condition variable
 *
 * As an analogy, dog the thread wanted a access shared information(food)
 * he acquired apporpriate lock but then was disappointed to see that
 * the shared information was not ready yet
 * without anything else to do, he decided to sleep(or wait) on a
 * nearby condition variable until another thread updated the shared
 * information and woke him up.
 *
 *
 * condition variables functions
 * =============================
 * type - pthread_cond_t, must be initialized before they can be used
 *
 * Creating/Destroying:
 * ====================
 * pthread_cond_init(condition, attr) -  Dynamically created
 * pthread_cond_t = PTHREAD_COND_INITALIZER; (statically declared)
 * pthread_cond_destroy(condition)
 *
 * Waiting on condition:
 * =====================
 * pthread_cond_wait - will put the caller thread to sleep on the cond
 * variale and release the mutex lock, gurantees that when the
 * subsequent line is executed after the caller has woken up, the
 * caller will hold lock
 *
 * pthread_cond_timewait - place limit on how long it will block
 *
 * Waking thread based on condition:
 * =================================
 * pthread_cond_signal
 * pthread_cond_broadcase - wake up all threads blocked by the
 * specified condition variable
 * for all of the above functions, the caller must first hold the lock
 * that is associated with that condition variable
 *   > failure to do this can lead to several problems
 *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

pthread_mutex_t count_mutex      = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t condition_mutex  = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_cond     = PTHREAD_COND_INITIALIZER;

void *functionCount1();
void *functionCount2();
int count = 0;

#define COUNT_DONE 10
#define COUNT_HALT1 3
#define COUNT_HALT2 6

int main()
{
  pthread_t thread1, thread2;

  pthread_create(&thread1, NULL, &functionCount1, NULL);
  pthread_create(&thread2, NULL, &functionCount2, NULL);
  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);

  exit(0);
}

void *functionCount1()
{
  for(;;)
  {
    pthread_mutex_lock(&condition_mutex);

    while(count >= COUNT_HALT1 && count <= COUNT_HALT2)
    {
      pthread_cond_wait(&condition_cond, &condition_mutex);
    }

    pthread_mutex_unlock(&condition_mutex);

    pthread_mutex_lock(&count_mutex);
    count++;
    printf("Counter value %s: %d\n", __func__, count);
    pthread_mutex_unlock(&count_mutex);

    if(count >= COUNT_DONE) return (NULL);
  }
}

void *functionCount2()
{
  for(;;)
  {
    pthread_mutex_lock(&condition_mutex);

    if(count < COUNT_HALT1 || count > COUNT_HALT2)
    {
      pthread_cond_signal(&condition_cond);
    }
    pthread_mutex_unlock(&condition_mutex);

    pthread_mutex_lock(&count_mutex);
    count++;
    printf("Counter value %s: %d\n", __func__, count);
    pthread_mutex_unlock(&count_mutex);

    if(count >= COUNT_DONE) return (NULL);
  }
}

