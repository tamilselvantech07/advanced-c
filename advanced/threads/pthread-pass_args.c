#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<string.h>
#include<stdlib.h>

#if 0
// Example 1: Pass single argument

void *print_message(void *ptr);

int main()
{
	pthread_t thread1, thread2;
	char *message1 = "Thread 1";
	char *message2 = "Thread 2";
	int iret1 = 0, iret2 = 0;

	iret1 = pthread_create( &thread1, NULL, print_message, (void*) message1);
	iret2 = pthread_create( &thread2, NULL, print_message, (void*) message2);
/* wait till threads are complete before main continues. Unless we
	 wait we run the risk of executing an exit which will terminate
	 the process and all threads before the threads have completed */

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);

	printf("Thread 1 returns: %d\n", iret1);
	printf("Thread 2 returns: %d\n", iret2);
	pthread_exit(NULL);
	exit(0);
}

void *print_message(void *ptr)
{
	char *message;
	message = (char *)ptr;
	printf("%s \n", message);
}
#endif

// Example 2: pass more arguments

#if 0
struct thread_data
{
	int thread_id;
	int sum;
	char *message;
};

void *printHello(void *thread_arg)
{
	struct thread_data *my_data;
  int taskid = 0, sum = 0;
	char hello_msg[255];

	my_data = (struct thread_data *) thread_arg;
	taskid = my_data->thread_id;
	sum = my_data->sum;
	strcpy(hello_msg, my_data->message);
	printf("taskid = %d, sum = %d, message = %s\n", taskid, sum, hello_msg);
}

int main()
{
	pthread_t my_thread;
	struct thread_data my_thread_data;

	my_thread_data.message = malloc(sizeof(char) * 255);

	my_thread_data.thread_id = 10;
	my_thread_data.sum = 35;
	my_thread_data.message = "Hello world";

	pthread_t my_id  = pthread_create(&my_thread, NULL, printHello, (void *) &my_thread_data);
	pthread_join(my_thread, NULL);
	pthread_exit(NULL);
	return 0;
}
#endif

// Note:
// when passing more that one argument, you can create a structure
// all arguments must be cast to (void *);


void *hello_return(void *args)
{
	char *hello = strdup("Hello World!\n");
	return (void *) hello;
}

int main(int argc, char args[])
{
	char *str;
	pthread_t thread;

  // create a new thread that runs without arguments
	pthread_create(&thread, NULL, hello_return, NULL);

  // wait until the thread completes, assign return value to str
	pthread_join(thread, (void **) &str);
	printf("pthread returned str: %s\n", str);
	pthread_exit(NULL);
	// if you prints the str after thread_exit, it will not be printed.

	return 0;
}

