#include<stdio.h>
#include<pthread.h>

void *hello_fun()
{
	printf("Hello World!\n");
	return NULL;
}

int main()
{
	pthread_t thread_id;
  pthread_create(&thread_id, NULL, hello_fun, NULL);

  // pthread_join wait for the thread to exit. if not, main() will be exited
	// before the thread exited.
  // main execution will pause at pthread_join until the thread provided as an 
  // argument has completed its execution
	pthread_join(thread_id, NULL);
	pthread_exit(NULL);

	return 0;
}
