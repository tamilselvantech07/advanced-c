/**********************************************************************
 * Overview:
 * mutexes - locks, block access to variables by other threads
 * joins - makes a thread wait till others are complete(terminated)
 * condition variable - a way to communicate to other threads
**********************************************************************/

/*********************************************************************
Mutexes:
mutexes are one way of synchronizing to shared resource
when protecting shared data, it is the programmer's responsibility
to make sure every thread that need to use a mutex does to
  > if four threads are updating the same data, but only one uses
   a mutex, the data can still be corrupted
very often the action performed by a thread owning a mutex is the
updating of global variables

a typical sequence in the use of a mutex as follows
 > create and initialize a mutex variable
 > several threads attempt to lock the mutex
 > only one succedds and that thread owns the mutex
 > the owner thread performs some set of actions
 > the owner unlocks the mutex
 > another thread acquires the mutex and repeats the process
 > finally the mutex is destroyed


when several threads comete for a mutex, the losers block at the call

please understand that a deadlock can occur when using a mutex lock
  > making sure threads acquire locks in an agreed order

================================
Creating and destroying mutexes:
================================
mutex variable must be declared with type pthread_mutex_t
 > they must be initialized/created before they can be used

thera are two ways to initialize/create a mutex variable
1. statically, when it is declared
  // pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
2. dynamically, with pthread_mutex_init() function
  // int pthread_mutex_init(pthread_mutex_t *mutex,
                            const pthread_mutexattr_t *mutexattr);
  attributes of the mutex can be give thorugh second param(pass NULL
  as default)

the pthread standard defines three optional attributes
 > protocol - specifies the protocol used to preven prority inversions
 for a mutex
 > prioceiling - specifies the priority ceiling of a mutex
 > process-shared - specifies the process sharing of a mutex

the pthread_mutex_destroy(mutex) function should be used to free a
mutex object which is no longer needed.

==============================
Locking and unlocking mutexes:
==============================

int pthread_mutex_lock(pthread_mutex_t *mutex);
  > if the mutex is already locked by another thread, this call will
  block the calling thread until the mutex is unlocked

int pthread_mutex_trylock(pthread_mutex_t *mutex);
  > will attempt to lock a mutex, however, if the mutex is already
  locked, the routine will return immediately with a "busy" error code
  > maybe useful in preventin deadlock condtions, as in a
  priority-inversion situation

int pthread_mutex_unlock(pthread_mutex_t *mutex);
  > will unlock a mutex if called by the owning thread
  > an error will be returned if
    > if the mutex was already unlocked
    > if the mutex is owned by another thread

anytime a global resource is accessed by more than one thread the
resource should have a mutex associated with it.
**********************************************************************/



#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

// Example 1
#if 0
pthread_mutex_t lock = PTHREAD_COND_INITIALIZER;

int j = 0;

void do_process()
{
  // static mutex lock
  pthread_mutex_lock(&lock);
  int i = 0;
  j++;

  while (i < 5)
  {
    printf("%d", j);
    sleep(1);
    i++;
  }
  printf("...Done\n");
  pthread_mutex_unlock(&lock);
}

int main(void)
{
  int err = 0;
  pthread_t t1, t2;

  // dynamic mutex lock
  if(pthread_mutex_init(&lock, NULL) != 0)
  {
    printf("Mutex initialization failed.\n");
    return 1;
  }

  j = 0;

  pthread_create(&t1, NULL, do_process, NULL);
  pthread_create(&t2, NULL, do_process, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  return 0;
}
#endif

// Example 2

#define NTHREADS 10

void *thread_function(void *);
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

int counter = 0;

int main(void)
{
  pthread_t thread_id[NTHREADS];
  int i = 0, j = 0;

  for(i = 0; i < NTHREADS; i++)
  {
    pthread_create(&thread_id[i], NULL, thread_function, NULL);
  }

  for(j = 0; j < NTHREADS; j++)
  {
    pthread_join(thread_id[j], NULL);
  }
  /* Now that al thread are complete I can print the final result
  without the join I could be printing a value before all the*/
  printf("Final counter value: %d\n", counter);
}

void *thread_function(void *dummyPtr)
{
  printf("Thread number %ld\n", pthread_self());
  pthread_mutex_lock(&mutex1);
  counter++;
  pthread_mutex_unlock(&mutex1);
}