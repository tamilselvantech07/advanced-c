#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include<time.h>

#ifdef DEBUG
#define LOG(level, format, ...) { \
	if(level <= DEBUG){ \
	time_t curtime = time(NULL); \
	struct tm *ltm = localtime(&curtime);\
	printf("[%d-%02d-%02d %02d:%02d:%02d] %s:%s:%d " format, \
        ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday, \
        ltm->tm_hour, ltm->tm_min, ltm->tm_sec, \
	    __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
}}
#else
#define LOG(format, ...)
#endif

#endif
