// In C, bool type is not a buil-in data type, like int or char.
// It was introduced in 1999, you must import stdbool.h file to use it.
//

#include<stdio.h>
#include<stdbool.h>

int main(){
  bool isProgrammingFun = true;
  bool isFishTasty = false; 
  printf("%d\n", isProgrammingFun);
  printf("%d\n", isFishTasty);
}
