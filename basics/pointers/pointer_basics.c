#include<stdio.h>

void swap(int *a, int *b);

int main(void)
{

	int a = 5; 
	int *b;  // pointer variable, it could store memory address.

	b = &a; // storing address of variable a;
	printf("%p\n", &a);
	printf("%p\n", b);
	
	printf("%d\n", a);
	printf("%d\n", *b); //Derefrencing the pointer by putting *;

	*b = 10;   // modifying value by derefrencing the pointer variable.
	//So a will be 10.

	int x, y;
	x = 5;
	y = 10;
	printf("x: %d, y: %d\n", x, y);
  swap(&x, &y);  // Pass by reference as we're passing the memory address
	// Note: Technically, C does not have pass by reference, we're simulating
	// pass by reference by passing pointers.
  //swap(x,y);i.e swap(5,10); // pass by values. it will not return swapped values. 
	printf("x: %d, y: %d\n", x, y);

	return 0;
}

void swap(int *a, int *b)
{
	int temp = 0;
	temp = *a;   // Derefrencing the pointer a. So, temp = 5;
	*a = *b;
	*b = temp;
}


