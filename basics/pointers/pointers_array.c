#include<stdio.h>

int main()
{
	int numbers[4]={25,30,35,40};
	
	for (int i=0; i<4; i++)
	{
		printf("%d\n", numbers[i]);
	}

	for(int j=0; j<4; j++)
	{
		printf("address of number: %p\n", &numbers[j]);  //0x7ff..f0, 0x7ff..f4..
	} // the number of each elements' memory is different,with an addition of 4


	printf("sizeof(numbers): %lu\n", sizeof(numbers)); //size of an int is 4 bytes *4 == 16

  // Well, in C, the name of an array, is actually a pointer to the first element of the array
	// Get the memory address of the numbers array
	printf("%p\n", numbers);		//0x7ff..f0

	// Get the memory address of the first array element
	printf("%p\n", &numbers[0]);		//0x7ff..f0

	// Since numbers array is a pointer to the first element in numbers, you can use the * operator to access it.
	printf("%d\n", *numbers);		//25

	// To access the reso fo the elements in numbers, you can increment the pointer/array (+1, +2, etc)
	//
	//Get the value of the second element in numbers
	printf("%d\n", *(numbers + 1)); 	//30
	
	// loop through it.
	for (int k=0; k<4; k++)
	{
		printf("%d\n", *(numbers + k));
	}

	// or
	int *ptr = numbers;
	for(int l=0; l<4; l++)
	{
		printf("%d\n", *(ptr + l));
	}	

	// It is also possible to change the value of array elements with points
	int mynums[4] = { 10,11,12,13};
	
	//change the value of the first element to 20
	*mynums = 20;

	//change the value of second element to 30
	*(mynums + 1) = 30;

	//Get the value of first and second element
	printf("%d\n",*mynums);
	printf("%d\n", *(mynums + 1));

}
