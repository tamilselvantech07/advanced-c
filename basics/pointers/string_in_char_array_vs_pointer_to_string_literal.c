#include<stdio.h>
#include<string.h>

int main()
{
	char s1[] = "abcdef"; // character array in a stack
	s1[0] = 'X';
	printf("s1: %s\n", s1);

	char *s2 = "abcdef";
	//s2[0] = 'Y'; // This will segment fault because of bad_access of memory
	// But pointer arithmetic is supported.
	s2++;  // This will points to next char 'b' (will skip 'a')

	// Note: You can't increment value type in char array like this(s1++)
	// Note: s1 will act like a constant pointer, but s2 is actully a pointer.
	printf("s2: %s\n", s2);

	// We cannot reassign another string to array type, except using strcpy.
	// But make sure that the size should be same, if not it will overflows.
	// s1 = "new string";
	strcpy(s1, "new st");  // should not exceeds the previous size 
	printf("s1: %s\n", s1);

  // Unlike array type string,  pointer string literal can be modified.
	s2 = "new string";
	printf("s2: %s\n", s2);


  printf("sizeof(s1): %ld\n", sizeof(s1));  // 7 (Including null terminator).
  printf("sizeof(s2): %ld\n", sizeof(s2));  // 8 (we expects 10, c stores that
	// s2 pointer on stack.  So that first assignment is to be considered.
	// size for the pointer variables are always 8

	char *string = "helloworld!";
	int *array;
	printf("sizeof(char *string) = %lu\n", sizeof(string)); // 8
	printf("sizeof(int *array) = %lu\n", sizeof(array));    // 8

	return 0;
}
