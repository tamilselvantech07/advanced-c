//A pointer is a variable that stores the memory address of another variable as its value.
//A pointer variable points to a data type (like int) of the same type, and is created with the * operator.
//The address of the variable you are working with is assigned to the pointer:

#include <stdio.h>

int main() {
  int myAge = 43;  // Variable declaration
  int *ptr = &myAge;  // Pointer declaration.  (Stores the address of another variable)

  // Reference: Output the memory address of myAge with the pointer (0x7ffe5367e044)
  printf("%p\n", ptr);

  //output the mermory address of myAge (0x7ffe..)
  printf("%p\n", &myAge); 
  

  // Dereference: Output the value of myAge with the pointer (43)
  printf("%d\n", *ptr);

  // Example 2
  int *ptr2 = myAge; // throws warning: initialization of ‘int *’ from ‘int’ makes pointer from integer without a cast [-Wint-conversion]
	// Note: pointers declared with memory address of another variable except arrays.
	// Because, arrays variable itself address to the first value of the array.
  printf("%p\n",ptr2); // some other garbage address will be printed 0x2b
  printf("%p\n",&myAge); // output the mermory address of myAge (0x7ffe..) 
  printf("%p\n",myAge);  // warning: format ‘%p’ expects argument of type ‘void *’, but argument 2 has type ‘int’ [-Wformat=]
	// Note: %p always expects pointer variable or memory address. 
  printf("%d\n",*ptr2);  // Segmentation fault (core dumped).As because *ptr2 = myAge;

  return 0;
}


/*
Note that the * sign can be confusing here, as it does two different things in our code:

When used in declaration (int* ptr), it creates a pointer variable.
When not used in declaration, it act as a dereference operator.
Good To Know: There are two ways to declare pointer variables in C:

int* myNum;
int *myNum;

Notes on Pointers:
Pointers are one of the things that make C stand out from other programming languages, like Python and Java.
 */

