#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct node
{
	int value;

	struct node *next;
} NODE;

void print_ll(NODE **m1)
{
	printf("Print Linked List:");
	NODE *curr = *m1;

	while(curr != NULL)
	{
		printf("%d ", curr->value);
		curr = curr->next;
	}
	printf("\n");
}

// Method 1
void rec_remove_an_item(NODE **m1, int value)
{
	printf("Method:1 Recursive Remove an item\n");
	if(m1 == NULL)
	{
		return;
	}

	NODE *curr = *m1;
	if(curr->value ==  value)
	{
		puts("Head Matches");
		printf("%d \n ", curr->value);
		*m1 = curr->next;
		free(curr);
		curr = NULL;
		return;
	}
	remove_an_item(&((*m1)->next), value);
}


// Method 2

void remove_an_item(NODE **m1, int value)
{
	printf("Method:2 Remove an item\n");
	NODE *curr = *m1;
	NODE *prev = NULL;

	if(curr != NULL && curr->value == value)
	{
		printf("Item %d to be deleted \n ", curr->value);
		*m1 = curr->next;
		free(curr);
		return;
	}

  while(curr != NULL && curr->value != value)
	{
		prev = curr;
		curr = curr->next;
	}

	if(curr == NULL)
	{
		return;
	}

  prev->next = curr->next;
	free(curr);
}

//Method 3
void while_remove_an_item(NODE **m1, int value)
{
	printf("Method:3 while Remove an item\n");
	NODE *curr = *m1;
	NODE *prev = NULL;

	while(curr != NULL)
	{
	  if(curr->value == value)
		{
			puts("Head Matched");
			printf("Item %d to be deleted \n ", curr->value);
			*m1 = curr->next;  // move the head pointer;
			free(curr);
			return;
		}
		else
		{
			puts("Head not matched");
		  prev->next = curr->next;
		}
		prev = curr;
		curr = curr->next;
	}
	return;
}


int main(void)
{
	NODE *m1=NULL;
	NODE *m2=NULL;
	NODE *m3=NULL;

	m1 = malloc(sizeof(NODE));
	printf("m1: %p\n", m1);
	m1->value = 5;
	m2 = malloc(sizeof(NODE));
	printf("m2: %p\n", m2);
	m1->next = m2;

	m2->value = 7;
	m3 = malloc(sizeof(NODE));
	printf("m3: %p\n", m3);
	m2->next = m3;

	m3->value = 9;
	m3->next = NULL;

  remove_an_item(&m1, 9);
	print_ll(&m1);
	return 0;
}
