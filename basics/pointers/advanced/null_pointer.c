#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
	int value;

	struct node *next;
} NODE;


int main(void)
{
	int *ptr;

//	printf("ptr: %p\n", ptr);   // It will show some address,
//	printf("*ptr: %d\n", *ptr); // but when derefrencing, it could crashes
  	                            // the program or	return garbage value.

	/**********************************************************************
	* Printing an uninitialized value is undefined behavior.
	* On some platforms, you will get 0. On others, you will get "garbage"
	* values that may or may not change with each run.
	**********************************************************************/

	int *null_ptr = NULL;
  printf("null_ptr: %p\n", null_ptr);  // null_ptr: (nil)

	null_ptr = malloc(sizeof(int));

	if(null_ptr != NULL)
	{
		*null_ptr = 5;  // if memory allocated successfully, then we can initialize
		printf("*null_ptr: %d\n", *null_ptr);
  }
	else
	{
		puts("Memory allocation is failed");
	}

	printf("\nbefore free:\n%p\n", null_ptr); // 0xdd
	free(null_ptr);
	printf("\nafter free:\n%p\n", null_ptr);  // 0xdd
	null_ptr = NULL;
	printf("\nafter setting NULL:\n%p\n", null_ptr);  //(nil) 

	/***********************************************************************
	 * So, freeing the pointer does not means that is going to be NULL.
	 * we have to NULL it explicitly after freeing the memory address.
	 * if we don't do this, it could be the "dangling pointer".
	 * If we Nullifying and then freeing the memory is won't free the memory.
	 ************************************************************************/


  // e.g 1

	NODE n1, n2, n3;
	printf("n1: %p\n", &n1);
	n1.value = 5;
	printf("n2: %p\n", &n2);
	n1.next = &n2;
	n2.value = 7;
	printf("n3: %p\n", &n3);
	n2.next = &n3;
	n3.value = 9;
	n3.next = NULL;

	printf("Linked List:");
  NODE *traverse = &n1;

	while(traverse != NULL)
	{
		printf("%d ", traverse->value);
		traverse = traverse->next;
	}
	printf("\n");


  int *array = malloc(sizeof(int) * 10);

	printf("\nbefore array:\n%p\n", array);  // 0xdd
	array = realloc(array, sizeof(int) * 11);
	printf("\nafter array:\n%p\n", array);   // 0xdd. points same address, but the
																				   // block of memory is one int larger.

  array = realloc(NULL, sizeof(int) * 11); // 0xde. It moves to point next
																					 // block of memory.

  free(array);
	array = NULL;


	// Note: size of NULL and int* is same: 8
	printf("sizeof(NULL): %zu\n", sizeof(NULL));
	printf("sizeof(int*): %zu\n", sizeof(int*));

  char *char_pointer = NULL;
	int *int_pointer = NULL;

	if(char_pointer == int_pointer)
		printf("NULL pointer equal\n");

	// C99 onwards NULL is defined as ((void *)0)
	// i.e. 0 converted to a void pointer
	// NULL == 0 must evaluate to true
	return 0;
}
