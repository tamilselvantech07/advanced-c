#include<stdio.h>
#include<stdlib.h>


void function(int a, int b)
{
	printf("%d+%d = %d\n", a, b, a+b);
}



int main(void)
{
	int (*pointer_array)() = &function;

	(*pointer_array)(20, 5);
	return 0;
}
