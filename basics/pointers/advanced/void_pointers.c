#include<stdio.h>

int main()
{
	int a = 10;
	double b = 20;

	int *x = &a;
	printf("x: %d\n", *x);  // we can dereference the pointer like this.

  // int *y = &b; // -Wincompatible-pointer-types.
	// initialization of ‘int *’ from incompatible pointer type ‘double *’


  // But with void pointer, we can initialize from any pointer type.
	void *p;
  
	p = &a;
	p = &b;

  // void *malloc(size_t size) is an example.
	// int *a = malloc(sizeof(int) * 10);
	// But in c++, you have to type cast it. int *a = (int *)malloc(sizeof(int));
	// In C, we don't need to type cast it.

  // Note: But derefrencing a void pointer is not easy.  You've to cast it.
	// printf("p: %f\n", *p); // throws an error. Augment type 'void' is incomplete.
  printf("p: %f\n", *((double *) p));


  // e.g

	char name[] = "tomo";
	char *c = &name[1];
	printf("c: %c\n", *c);

	c = c + 1; // It will not add 1 to the address, 
	           // will advance the memory address to next;

	printf("c: %c\n", *c);  // o/p: m
	
  // Pointer arithmetic is not officialy supported in void pointer
	// as per the standards.  But some compilers supports it.

	p = &name[1];
	p = p + 1;
	printf("p: %c\n", *((char *) p));  // o/p: m

	return 0;
}
