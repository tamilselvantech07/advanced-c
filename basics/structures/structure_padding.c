#include<stdio.h>

// Method 2 to avoid structure padding
// #pragma pack(1) 

struct data
{
	long long a;
	char x;
	int y;
	char z;
}; // __attribute__((__packed__));   <-- Method 3 

// only char x and int y is defined.
// Address   Data
// 
//  0x00      x
//  0x01      padding
//  0x02      padding
//  0x03      padding
//  0x04      y
//  0x05      y
//  0x06      y
//  0x07      y
//  0x08      z
//  0x09      padding
//  0x0A      padding
//  0x0B      padding

// CPU reads word by word, so it pads other datas to read each data member in a
// single read.
 

int main(void)
{
	printf("sizeof(int):%zu\n", sizeof(int)); //4
	printf("sizeof(char):%zu\n", sizeof(char)); //1
	
  // So, we expecting the size of data could be 6.
	printf("sizeof(struct data):%zu\n", sizeof(struct data)); // 12
	
	// Method: 1
	// To avoid this padding, define the biggest data type first.
	// int x, char y, char z;    In this case, the size will be 8.

// Address   Data
// 
//  0x00      x
//  0x01      x
//  0x02      x
//  0x03      x
//  0x04      y
//  0x05      z
//  0x06      padding
//  0x07      padding

	return 0;
}
