#include<stdio.h>
#include<string.h>

struct myStructure {
	int mynum;
	char myletter;

	char mystring[30]; // string. you can't directly assign string by calling this member
};


int main(){
	// Unlike an array, a structure can contain many different data types.
	struct myStructure s1;
	
	// Assign values to members of s1
	s1.mynum = 10;
	s1.myletter = 'T';

	printf("My number: %d\n", s1.mynum);
	printf("My letter: %d\n", s1.myletter);

	struct myStructure s2;

	// Assing values to different struct variables;
	s2.mynum = 20;
	s2.myletter = 'U';
	printf("My number: %d\n", s2.mynum);


	// Trying to assing a value to the string
//	s1.mystring = "some text";  // error: assignment to expression with array type

	// However, you can use strcpy() function and assin value to s1.mystring
	strcpy(s1.mystring, "Some Test");
	printf("My string: %s\n", s1.mystring);
 	
	// You can also assing values to members of a structure variable at declaration time, in a single line.
	struct myStructure s3 = {30, 'X', "Another method"};

	// Modify values
	s3.mynum = 31;
	strcpy(s3.mystring, "Method 2");

	printf("%d %c %s\n", s3.mynum, s3.myletter, s3.mystring);

	// Create a structure variable
	struct myStructure st1 = {13, 'B', "Some text"};

	// Create another structure variable
	struct myStructure st2;	

	// copy st1  values to st2
	st2 = st1;

	// changes st2 values
	st2.mynum = 23;
	st2.myletter = 'C';
	strcpy(st2.mystring, "Changing values");
	
	// Print values
	printf("%d %c %s\n", st1.mynum, st1.myletter, st1.mystring); // 13 B Some text
	printf("%d %c %s\n", st2.mynum, st2.myletter, st2.mystring); // 23 C Changing values

}
