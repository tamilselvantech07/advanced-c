#include<stdio.h>
#include<stdlib.h> //malloc
#include<string.h>

#if 0
Structures (also called structs) are a way to group several related variables
into one place. 
Each variable in the structure is known as a member of the structure.

Unlike an array, a structure can contain many different data types
(int, float, char, etc.).

struct MyStructure {   // Structure declaration
  int myNum;           // Member (int variable)
  char myLetter;       // Member (char variable)
}; // End the structure with a semicolon

typedef int Interger; // Integer is an alias for int data type. 
#endif

typedef struct
{
	char name[30];
	char id[10];
	int age;
	int grades[5];
}Student;

typedef struct
{
	int x;
	int y;
} Point;

typedef struct
{
	int *array;  // pointer member
}Data;


void print_student(Student student); // Declaration of a function
void print_points(Point points[]);	 // Declaration of a function  
// void print_points(Point points[10]);
// void print_points(Point *points);  // both will work

int main(void)
{
	// To access the structure, you must create a variable of it.
  // struct Student tamil;  // usual way to create a variable for structure
														// by using struct keyword.


  Student tamil;  // with the help of typedef alias we can directly
									// create a variable without using struct keyword

  // Intializing struct members
  strcpy(tamil.name, "Tamilselvan");
  strcpy(tamil.id, "123");
  tamil.age = 24;
	tamil.grades[0] = 1;
	tamil.grades[1] = 2;
	tamil.grades[2] = 3;
	tamil.grades[3] = 4;
	tamil.grades[4] = 5;

  print_student(tamil);

  // Intializing struct members
	Point p1 = {5, 10};  					// way 1 (in same order)
	Point p2 = {.y = 2, .x = 8};  // way 2 (in any order with member prefix)

	printf("p1.x: %d\n", p1.x);
	printf("p1.y: %d\n", p1.y);

	printf("p2.x: %d\n", p2.x);
	printf("p2.y: %d\n", p2.y);
  
	// Copy Structures
	p1 = p2; // basically a memcpy
	puts("p1 = p2");
	printf("p1.x: %d\n", p1.x);
	printf("p1.y: %d\n", p1.y);

  Point points[10];

  // Initializing struct member arrays
  for(int j=0; j<10; j++)
	{
		points[j].x = j;
		points[j].y = 10-j;
	}
  
	print_points(points);

 
  Data x;
	Data y;

	x.array = malloc(sizeof(int) * 5);
	y.array = malloc(sizeof(int) * 5);

	x.array[0] = 1;
	x.array[1] = 2;
	x.array[2] = 3;
	x.array[3] = 4;
	x.array[4] = 5;


	y.array[0] = 9;
	y.array[1] = 9;
	y.array[2] = 9;
	y.array[3] = 9;
	y.array[4] = 9;

  x = y;

// Data x struct is not storing the array, it is storing the pointer to array.
// Note: Here it is not copying the structure variable of x's address to y.
// Copying the pointer array address of the structure that is dynamically
// allocated.


  printf("x.array = %p\n", x.array);  // 0x5614b9fe66d0
  printf("y.array = %p\n", y.array);  // 0x5614b9fe66d0

	for(int l=0; l<5; l++)
	{
		printf("x.array[%d] = %d\n", l, x.array[l]);
	}

  // one more example
	x.array[0] = 10;
  printf("y.array[0] = %d\n", y.array[0]);

  free(x.array);
  //free(y.array); // It will throw an error as both are pointing the same
									 // pointer variable/member;
	                 // free(): double free detected in tcache 2
	return 0;
}

void print_student(Student stu) // Defining the function
{
  printf("Name: %s\n", stu.name);
  printf("ID: %s\n", stu.id);
  printf("Age: %d\n", stu.age);
  int length = sizeof(stu.grades)/sizeof(stu.grades[0]);
	for(int i=0;i<length;i++)
	{
		printf("Grade[%d]: %d\n", i, stu.grades[i]);
	}
}

void print_points(Point pnts[])
{

	for(int k=0;k<10;k++)
	{
		printf("point[%d] = (%d,%d)\n", k, pnts[k].x, pnts[k].y);
	}
}
