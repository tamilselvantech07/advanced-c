#include<stdio.h>

struct complex{
	int real;
	int imag;
};

struct complex add(struct complex a, struct complex b){
	struct complex d;
	d.real = a.real+b.real;
	d.imag = a.imag+b.imag;
	return d;
};

struct complex sub(struct complex a, struct complex b){
	struct complex d;
	d.real = a.real+b.real;
	d.imag = a.imag+b.imag;
	return d;
};

struct complex multiply(struct complex a, struct complex b)
{
  struct complex d;
  d.real = (a.real*b.real)-(a.imag*b.imag);
  d.imag = (a.real*b.imag)+(a.imag*b.real);
  return d;
};

int main(){
	struct complex d1 = {12,2};
	struct complex d2 = {14,11};

	struct complex a = add(d1,d2);
	struct complex s = sub(d1,d2);
	struct complex m = multiply(d1,d2);

	
 	printf("ADD - %d+%di\n",a.real,a.imag);
  	printf("SUB - %d+%di\n",s.real,s.imag);
  	printf("MUL - %d+%di\n",m.real,m.imag);

  	return 0;
}
