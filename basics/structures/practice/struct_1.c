#include<stdio.h>

struct student {
	int roll_no;
	char name[30];
	int age;
	int marks;
};

int main(){
	struct student s1 = {1,"Tamil",22,456};
	printf("rollno:\t%d\nname: \t%s\nage: \t%d\nmarks: \t%d\n",s1.roll_no,s1.name,s1.age,s1.marks);
	return 0;
}	
