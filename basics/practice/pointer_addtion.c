#include<stdio.h>
#include<stdlib.h>

void update(int *a, int *b){
	*a = *a+*b;
	*b = abs(*a-*b-*b);
}

int main(){
	int a,b;
	int *pa = &a,  *pb = &b;
	scanf("%d %d", &a, &b);
	update(pa, pb);
	printf("a=%d b=%d\n", a, b);
}
