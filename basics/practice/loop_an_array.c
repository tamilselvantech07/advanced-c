#include <stdio.h>

int main() {
  int myNumbers[] = {25, 50, 75, 100};
  int i;

  printf("%ld\n", sizeof(myNumbers));
  
  size_t size = sizeof(myNumbers) / sizeof(myNumbers[0]);

  for (i = 0; i < size; i++) {
    printf("%d\n", myNumbers[i]);
  }
 
  return 0;
}
