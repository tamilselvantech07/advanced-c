#include <stdio.h>
#include<string.h>

int main() {
  char greetings[] = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!', '\0'};
  char greetings2[] = "Hello World!";
  
  printf("%s\n", greetings);
  printf("%s\n", greetings2);
  
  int i;

  // Find length of a string
  // Method 1:  usally we use this method to find length of an array.
  size_t size = sizeof(greetings) / sizeof(greetings[0]);

  // Method 2: we can use strlen() for finding legnth of an array of string
  int size2 = strlen(greetings);
  printf("%d\n", size2);

  // loop strings
  for(i=0; i<size; ++i){
	  printf("%c\n", greetings[i]);
  }
  return 0;
}
