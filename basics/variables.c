// Types of variables in c.
// 1. local variable -- declared inside the function or block
// 2. global variable -- declared outside the function or block.
// 3. static variable -- declared with the 'static' keyword.  It retains its values between multiple times.
// 4. automatic variable -- all variables in c that declared inside the block, are automatic variables by default. we can explicitly declare an automatic var using 'auto' keyword.
// 5. external variable -- we can share a variable in multiple C source files by using an external variable. To declare an external var, use 'extern' keyword.
//
#include<stdio.h>
int a=10; //global variabe

void function1(){
	int b = 10;
	static int c = 10; // static variable
	b = b+1;
	c = c+1;
	printf("%d,%d\n",b,c);  // if you call this fn many times the local variable b will print the same value (11,11,11),
						  // but static var c will print incremented value(11,12,13)
}
int main(){
	int d=11; //local variable(also automatic)
	auto int e = 20;//automatic variable

	function1();
	function1();
	function1();
}


// External variable
// We can share a variable in multiple C source files by using an external variable. To declare an external variable, you need to use extern keyword.

//>myfile.h
//extern int x=10;//external variable (also global)


//>program1.c
//#include "myfile.h"
//#include <stdio.h>
//void printValue(){
//    printf("Global variable: %d", global_variable);
//}

