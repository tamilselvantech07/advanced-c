#include<stdio.h>

int main(){
	FILE *fptr;

	fptr = fopen("a.txt","r");
	
	// Print some text if the file does not exist
	if(fptr != NULL) {
	
		// Store the content of the file
		char string[100];

		// Reads 1st line of the file only	
		//fgets(string, 100, fptr);

		//printf("%s\n", string);

		//Read the content of entire file and print it
		while(fgets(string, 100, fptr)){
			printf("%s", string);
		}
	} else {
		printf("Not able to open the file.");
		
	}
	fclose(fptr);
}
