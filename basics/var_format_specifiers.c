#include<stdio.h>

int main(){
  int myNum = 10;
  float myFloatNum = 5.99;
  char myLetter = 'a';
  // char arr = "tamil"; // will throw segmentation fault with %s,
                         // won't print with %c
  char array[] = "tamil";

  printf("%d\n", myNum);
  printf("%f\n", myFloatNum);
  printf("%c\n", myLetter);
  printf("%s", array);
 }
