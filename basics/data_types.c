#include<stdio.h>

//Data Types	     Memory Size  Range
//char			1 byte	−128 to 127
//signed char		1 byte	−128 to 127
//unsigned char		1 byte	0 to 255
//short			2 byte	−32,768 to 32,767
//signed short		2 byte	−32,768 to 32,767
//unsigned short	2 byte	0 to 65,535
//int			2 byte	−32,768 to 32,767
//signed int		2 byte	−32,768 to 32,767
//unsigned int		2 byte	0 to 65,535
//short int		2 byte	−32,768 to 32,767
//signed short int	2 byte	−32,768 to 32,767
//unsigned short int	2 byte	0 to 65,535
//long int		4 byte	-2,147,483,648 to 2,147,483,647
//signed long int	4 byte	-2,147,483,648 to 2,147,483,647
//unsigned long int 	4 byte	0 to 4,294,967,295
//float			4 byte	
//double		8 byte	
//long double		10 byte

int main(){
	int a;
	printf("%ld\n",sizeof(a));
}
