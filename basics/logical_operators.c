#include<stdio.h>

// logical operators returns 0(false) or 1(true).

int main(){
  int x = 7;
  int y = 3;
  int z = 0;
  printf("%d \n", x<5 && x<10); // 0
  printf("%d \n", x<5 || x<10); // 1
  printf("%d\n", !x);          // 0 
  printf("%d \n", !z);          // 1
}
