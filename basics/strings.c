//
// Unlike many other programming languages,
// C does not have a String type to easily create string variables.
// Instead, you must use the char type and create an array of characters
//  to make a string in C:

#include <stdio.h>

int main() {
  char greetings[] = "Hello World!";
  printf("%s", greetings);

  return 0;
}
