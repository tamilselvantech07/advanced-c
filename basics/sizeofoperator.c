#include<stdio.h>

int main(){
  int myInt;
  float myFloat;
  double myDouble;
  char myChar;

  printf("int %lu\n", sizeof(myInt));
  printf("float %lu\n", sizeof(myFloat));
  printf("double %lu\n", sizeof(myDouble));
  printf("char %lu\n", sizeof(myChar));
  // we use the %lu format specifier. because the compiler expects
  // the size of operator to return a long unsigned int(%lu), instead
  // int(%d). 
}
