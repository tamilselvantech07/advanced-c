#include<stdio.h>



// By default, the first item(LOW) has the value 0, the second(MEDIUM) has the value 1, etc.

enum Level {
	LOW,
	MEDIUM,
	HIGH
};

enum Speed {
	// To make more sense of the values, you can change them
	SLOW = 25,
	MODERATE = 50,
	FAST = 75
};
	
enum Color {
	Red = 5,
	Green,	// Now 6
	Yellow  // Now 7 because the next items will update their numbers accordingly.
};

int main(){

	// Create an enum variable and assign a value to it.
	enum Level myvar = MEDIUM;
	enum Speed mysp = FAST;

	printf("%d\n", myvar);    // 1
	printf("%d\n", mysp);	  // 75

	return 0;
}

